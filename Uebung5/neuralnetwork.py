import mxnet as mx
import numpy as np

groesse_vektor = 20

"""" Metrics with ignore label """

class Accuracy(mx.metric.EvalMetric):
    """
    Calculates accuracy. Taken from MXNet and adapted to work with batch-major labels
    (reshapes (batch_size, time) -> (batch_size * time).
    Also allows defining an ignore_label/pad symbol
    """

    def __init__(self,
                 name='accuracy',
                 output_names=None,
                 label_names=None,
                 ignore_label=None):
        super(Accuracy, self).__init__(name=name,
                                       output_names=output_names,
                                       label_names=label_names,
                                       ignore_label=ignore_label)
        self.ignore_label = ignore_label

    def update(self, labels, preds):
        mx.metric.check_label_shapes(labels, preds)

        for label, pred_label in zip(labels, preds):
            if pred_label.shape != label.shape:
                pred_label = mx.nd.argmax_channel(pred_label)
            pred_label = pred_label.asnumpy().astype('int32')
            label = mx.nd.reshape(label, shape=(pred_label.size,)).asnumpy().astype('int32')

            mx.metric.check_label_shapes(label, pred_label)
            if self.ignore_label is not None:
                correct = ((pred_label.flat == label.flat) * (label.flat != self.ignore_label)).sum()
                ignore = (label.flat == self.ignore_label).sum()
                n = pred_label.size - ignore
            else:
                correct = (pred_label.flat == label.flat).sum()
                n = pred_label.size

            self.sum_metric += correct
            self.num_inst += n

def batchsep(batches):
    first_loop = True
    for batch in batches:
        if first_loop:
            source_windows = batch[0]
            target_windows = batch[1]
            target_labels = batch[2]
            first_loop = False
        else:
            source_windows = mx.ndarray.concat(source_windows, batch[0], dim=0)
            target_windows = mx.ndarray.concat(target_windows, batch[1], dim=0)
            target_labels = mx.ndarray.concat(target_labels, batch[2], dim=0)
    return((source_windows,target_windows,target_labels))



def newneuro(train_data, train_label, val_data, val_label, batchsize, source_dict_len, target_dict_len,groesse_vektor, actfun, tmax, cpu, start_learning_rate, maxtrain):
    neuro = 5*groesse_vektor
    train_data = np.split(train_data, 2, axis=1)
    train_data = {'input_enc' : train_data[0], 'input_dec' : train_data[1]}
    train_iter = mx.io.NDArrayIter(train_data, train_label, batchsize)
    val_data = np.split(val_data, 2, axis=1)
    val_data = {'val_input_enc' : val_data[0], 'val_input_dec' : val_data[1]}
    val_iter = mx.io.NDArrayIter(val_data, val_label, batchsize)
    #Create NN
    enc = mx.sym.Variable('input_enc')
    dec = mx.sym.Variable('input_dec')
    enc = mx.sym.Embedding(data = enc, input_dim=source_dict_len, output_dim = groesse_vektor, name = 'embed_enc')
    dec = mx.sym.Embedding(data = dec, input_dim=target_dict_len, output_dim = groesse_vektor, name = 'embed_dec')
    enc = mx.sym.FullyConnected(enc, name = 'fc_enc', num_hidden=neuro, flatten=False)
    enc = mx.sym.Activation(enc, name='act_enc', act_type=actfun)
    dec = mx.sym.FullyConnected(dec, name = 'fc_dec', num_hidden=neuro, flatten=False)
    dec = mx.sym.Activation(dec, name='act_dec', act_type=actfun)
    net = mx.sym.concat(enc, dec)    
    lstm_cell = mx.rnn.LSTMCell(num_hidden=neuro)
    net, states = lstm_cell.unroll(length=int(2*tmax),inputs=net, layout='NTC', merge_outputs=True)
    net = mx.sym.split(net, axis=1, num_outputs=2)
    net = mx.sym.FullyConnected(net[1], name='proj', flatten=False,num_hidden=target_dict_len)
    net = mx.sym.SoftmaxOutput(net, name='softmax', use_ignore=True, ignore_label=0, preserve_shape=True)
    #for i in range(int(tmax)):
    #    net[int(tmax)+i] = mx.sym.FullyConnected(net[int(tmax)+i], name='proj'+str(i), num_hidden=target_dict_len)
    #    net[int(tmax)+i] = mx.sym.SoftmaxOutput(net[int(tmax)+i], name = 'target'+str(i), use_ignore=True, multi_output=True, ignore_label=0)
    
    #label_nam=[]
    #for i in range(int(tmax)):
    #    label_nam.append('target'+str(i)+'_label')
    if(cpu):
        mod=mx.mod.Module(symbol=net, context=mx.cpu(), data_names=['input_enc','input_dec'], label_names=['softmax_label'])
    else:
        mod=mx.mod.Module(symbol=net, context=mx.gpu(), data_names=['input_enc','input_dec'], label_names=['softmax_label'])
    mod.bind(data_shapes=train_iter.provide_data, label_shapes=train_iter.provide_label)
    mod.init_params(initializer=mx.init.Xavier(factor_type="in", magnitude=2.34))
    mod.init_optimizer(optimizer='adadelta', optimizer_params=(('learning_rate',start_learning_rate), ))
    metric_acc = Accuracy(ignore_label=0)
    metric_perp = mx.metric.Perplexity(ignore_label=0)
    epoch = 1
    new_perp = 0


 
    while(epoch<=maxtrain):
        train_iter.reset()
        val_iter.reset()
        metric_acc.reset()
        metric_perp.reset()
        for batch in train_iter:
            lstm_cell.reset()
            mod.forward(batch, is_train=True)
            preds = [pNTC.reshape((batchsize*int(tmax),target_dict_len)) for pNTC in mod.get_outputs()]
            labels = [lNT.reshape((batchsize*int(tmax),)) for lNT in batch.label]
            metric_acc.update(labels, preds)
            metric_perp.update(labels, preds)
            mod.backward()
            mod.update()
            print('Epoch %d, Training %s %s' % (epoch, metric_acc.get(), metric_perp.get()))
        
        
        
        
        epoch = epoch + 1
          



def neurotrain(train_batches, val_batches, batch_size, layercount, neurocount,source_dict_len, 
        target_dict_len,cpu,checkpoint_save_count,evaluate_val_count,isLoad,
        load_checkpoint_nr,is_reduce_learning_rate, start_learning_rate, actfun, opt, hlt,modelpath,maxtrain):
    groesse_vektor = int(source_dict_len/100)
    (train_source_window, train_target_window, train_target_label) = batchsep(train_batches)
    (val_source_window, val_target_window, val_target_label) = batchsep(val_batches)
    mx.random.seed(7777)
    train_target_label=train_target_label.T[0]
    val_target_label=val_target_label.T[0]
    data_nd = {'train_source_window' : train_source_window, 'train_target_window' : train_target_window}
    nd_iter = mx.io.NDArrayIter(data_nd, train_target_label, batch_size)
    data_val = {'val_source_window' : val_source_window, 'val_target_window' : val_target_window}
    val_iter = mx.io.NDArrayIter(data_val, val_target_label, batch_size)
    
    #Create NN
    embedding_net_source = mx.sym.Variable('train_source_window')
    embedding_net_target = mx.sym.Variable('train_target_window')
    embedding_net_source = mx.sym.Embedding(data = embedding_net_source, input_dim=source_dict_len, output_dim= groesse_vektor, name='el_source')
    embedding_net_target = mx.sym.Embedding(data = embedding_net_target, input_dim=target_dict_len, output_dim=groesse_vektor, name='el_target')
    fully_connected_left = mx.sym.FullyConnected(embedding_net_source, name='fcl', num_hidden=int(neurocount/5))
    fully_connected_left = mx.sym.Activation(fully_connected_left, name=actfun+'relul', act_type=actfun)
    fully_connected_right = mx.sym.FullyConnected(embedding_net_target, name='fcr', num_hidden=int(neurocount/10))
    fully_connected_right = mx.sym.Activation(fully_connected_right, name='relur', act_type=actfun)
    net = mx.sym.concat(fully_connected_left, fully_connected_right)
    net = mx.sym.FullyConnected(net, name='fc1', num_hidden=int(neurocount/2))
    #batch normalization
    
    for n in range(0,layercount):
        net = mx.sym.Activation(net, name='relu'+str(n), act_type=actfun)
        net = mx.sym.FullyConnected(net, name='fc'+str(n+2), num_hidden=neurocount)
        if(n==0):
            net = mx.sym.BatchNorm(net, name='batch_norm_layer1')
    #batch normalization
    #net = mx.sym.BatchNorm(net, name='batch_norm_layer2')
    net = mx.sym.Activation(net, name='relu', act_type=actfun)
    net = mx.sym.FullyConnected(net, name='projektions_layer', num_hidden=target_dict_len)
    net = mx.sym.SoftmaxOutput(net, name='softmax', multi_output=True, use_ignore=True, ignore_label=0)
    #digraph = mx.viz.plot_network(net, save_format = 'jpg')
    #digraph.render()
    model_prefix = modelpath+'/defaulttrainednn'
    print("Graph saved")



    if(cpu):
        mod=mx.mod.Module(symbol=net, context=mx.cpu(), data_names=['train_source_window','train_target_window'], label_names=['softmax_label'])
    else:
        mod=mx.mod.Module(symbol=net, context=mx.gpu(), data_names=['train_source_window','train_target_window'], label_names=['softmax_label'])
    if (isLoad):
        mod = mx.mod.Module.load(model_prefix, load_checkpoint_nr,False, data_names=['train_source_window','train_target_window'], label_names=['softmax_label'])
        #mod.set_params(arg_params=mod.get_params()[0], aux_params=mod.get_params()[1])

    mod.bind(data_shapes=nd_iter.provide_data, label_shapes=nd_iter.provide_label)
    if(not isLoad):
        mod.init_params(initializer=mx.init.Xavier(factor_type="in", magnitude=2.34))
    mod.init_optimizer(optimizer=opt, optimizer_params=(('learning_rate',start_learning_rate), ))
    metric_acc = mx.metric.create('acc')
    metric_perp = mx.metric.Perplexity(ignore_label=None)
    learning_rate=start_learning_rate  
    epoch = 1
    new_perp = 0
    learnratetest=float(hlt)
    while(epoch<=maxtrain):
        nd_iter.reset()
        val_iter.reset()
        metric_acc.reset()
        metric_perp.reset()
        for batch in nd_iter:
            mod.forward(batch, is_train=True)
            mod.update_metric(metric_acc, batch.label)
            mod.update_metric(metric_perp, batch.label)
            mod.backward()
            mod.update()
        print('Epoch %d, Training %s %s' % (epoch, metric_acc.get(), metric_perp.get()))
        
        
        
        if (epoch % checkpoint_save_count == 0):
            mx.model.save_checkpoint(model_prefix, epoch, net, mod.get_params()[0], mod.get_params()[1] )
        val_iter.reset()
        if (epoch % evaluate_val_count == 0):
            metric_acc.reset()
            metric_perp.reset()
            score = mod.score(val_iter, ["acc",metric_perp])
            print("Accuracy score on dev is %f" % (score[0][1]))
            print("Perplexity score on dev is %f" % (score[1][1]))
            
            old_perp = new_perp
            new_perp = score[1][1]   #metric_perp.get()
            if (epoch>1) and is_reduce_learning_rate and (abs(1-(old_perp/new_perp)) < learnratetest):
                learning_rate = learning_rate/2.
                learnratetest = learnratetest/2.
                mod.init_optimizer(optimizer_params=(('learning_rate',learning_rate), ),force_init=True)
                print("Changed learnrate")
            
        epoch = epoch + 1
          


def rnn_train(batchsize, source_dict_len, target_dict_len, train_data, train_label, train_length, val_data, val_label, val_length, encoder_dim, decoder_dim, tmax, cpu, isLoad, load_checkpoint_nrm, start_learning_rate, hl, maxtrain, checkpoint_save_count, evaluate_val_count,  modelpath, op, wpe):
    start_learning_rate = float(start_learning_rate)
    model_prefix = modelpath+'/defaulttrainedrnn'
    ############# Prepare Data ################################################
    mx.random.seed(8888)
    groesse_vector_source = int(source_dict_len/wpe)
    groesse_vector_target = int(target_dict_len/wpe)
    train_data = np.split(train_data, 2, axis=1)
    train_data = {'input_enc' : train_data[0], 'input_dec' : train_data[1], 'input_length' : train_length}
    train_iter = mx.io.NDArrayIter(train_data, train_label, batchsize)
    val_data = np.split(val_data, 2, axis=1)
    val_data = {'input_enc' : val_data[0], 'input_dec' : val_data[1], 'input_length' : val_length}
    val_iter = mx.io.NDArrayIter(val_data, val_label, batchsize)

    ############# Create NN ###################################################
    embedding_source = mx.sym.Variable('input_enc')
    embedding_target = mx.sym.Variable('input_dec')
    source_length = mx.sym.Variable('input_length')
    #shape = (batch_size, tmax)

    embedding_source = mx.sym.Embedding(data = embedding_source, name="embedding_enc", input_dim = source_dict_len, output_dim = groesse_vector_source)
    embedding_target = mx.sym.Embedding(data = embedding_target, name="embedding_dec", input_dim = target_dict_len, output_dim = groesse_vector_target)
    #shape = (batch_size, tmax, groesse_vector)
    #SR erwartet
    lstm_encoder = mx.rnn.LSTMCell(num_hidden = encoder_dim, prefix="lstm-encoder_")
    outputs_encoder, states_encoder = lstm_encoder.unroll(length = tmax, inputs = embedding_source, layout = 'NTC', merge_outputs = True)
    #outputs_encoder ist eine List von Symbolen mit shape = (batch_size, encoder_dim)
    #encoded_sentence = outputs_encoder[-1]
    #shape = (batch_size, encoder_dim)
    outputs_encoder = mx.sym.swapaxes(outputs_encoder, 0,1)
    encoded_sentence = mx.sym.SequenceLast(outputs_encoder, source_length, use_sequence_length=True)
    encoded_sentence = mx.sym.reshape(encoded_sentence, (batchsize,1,encoder_dim))
    encoded_sentence_stretched = mx.sym.tile(encoded_sentence, reps=(1,tmax,1))

    #single_encoded_sentence = encoded_sentence
    #for i in range(tmax - 1):

    #    encoded_sentence = mx.sym.concat(encoded_sentence, single_encoded_sentence, dim=0, name="concat"+str(i))
    
    #encoded_sentence_stretched = mx.sym.reshape(encoded_sentence, (batchsize, tmax, encoder_dim))
    ######## Das geht bestimmt nicht, weil man nicht über Symbols iterieren kann, also wie funktioniert das konkatenieren?
    #encoded_sentence_stretched = []
    #for batch in encoded_sentence:
    #   batch_stretched = []
    #   for word in range(0, tmax):
    #       batch_stretched.append(batch)
    #   encoded_sentence_stretched.append(batch_stretched)
    #shape von encoded_sequence_stretched = (batch_size, tmax, encoder_dim)

    input_decoder = mx.sym.concat(embedding_target, encoded_sentence_stretched, dim = 2)
    #shape = (batch_size, tmax, groesse_vector + encoder_dim)

    lstm_decoder = mx.rnn.LSTMCell(num_hidden = decoder_dim, prefix="lstm-decoder_")
    outputs_decoder, states_decoder = lstm_decoder.unroll(length = tmax, inputs = input_decoder, layout = 'NTC', merge_outputs = True)
    #shape = (batch_size, tmax, decoder_dim)

    ########## Brauchen wir hier eine Aktivierungsfunktion oder passiert das bereits im letzten Layer vom LSTM
    ########## Was genau macht flatten = True und was heißt der Tipp aus der Email?
    net = mx.sym.FullyConnected(outputs_decoder, name='projektionslayer', num_hidden = target_dict_len, flatten = False)
    #shape = (batch_size, tmax, target_dict_len)

    #Die Labels sollen shape (batch_size, tmax) haben. Das funktioniert aber nicht. preserve_shape = True heißt, dass softmax entland axis=-1 berechnet wird
    net = mx.sym.SoftmaxOutput(net, name = 'softmax', use_ignore=True, ignore_label=0, preserve_shape = True)

    ############# Prepare NN ###################################################
    if (isLoad):
        net, arg_params, aux_params = mx.rnn.load_rnn_checkpoint([lstm_encoder, lstm_decoder],model_prefix, load_checkpoint_nrm)
    if(cpu):
        mod=mx.mod.Module(symbol=net, context=mx.cpu(), data_names=['input_enc','input_dec','input_length'], label_names=['softmax_label'])
    else:
        mod=mx.mod.Module(symbol=net, context=mx.gpu(), data_names=['input_enc','input_dec','input_length'], label_names=['softmax_label'])
    
    mod.bind(data_shapes=train_iter.provide_data, label_shapes=train_iter.provide_label)
    if (isLoad):
        mod.set_params(arg_params=arg_params, aux_params=aux_params)
    else:
        mod.init_params(initializer=mx.init.Xavier(factor_type="in", magnitude=2.34))

    mod.init_optimizer(optimizer=op, optimizer_params=(('learning_rate',float(start_learning_rate)), ))
    metric_acc = Accuracy(ignore_label=0)
    metric_perp = mx.metric.Perplexity(ignore_label=0)
    epoch = 1
    if(isLoad):
        epoch = load_checkpoint_nrm +1
    new_perp = 0
    learnratetest=0.05

    while(epoch<=maxtrain):
        train_iter.reset()
        val_iter.reset()
        metric_acc.reset()
        metric_perp.reset()
        for batch in train_iter:
            lstm_encoder.reset()
            lstm_decoder.reset()
            mod.forward(batch, is_train=True)
            preds = [pNTC.reshape((batchsize*int(tmax),target_dict_len)) for pNTC in mod.get_outputs()]
            labels = [lNT.reshape((batchsize*int(tmax),)) for lNT in batch.label]
            metric_acc.update(labels, preds)
            metric_perp.update(labels, preds)
            mod.backward()
            mod.update()
        print('Epoch %d, Training %s %s' % (epoch, metric_acc.get(), metric_perp.get()))
        
        
        if (epoch % checkpoint_save_count == 0):
            mx.rnn.save_rnn_checkpoint([lstm_encoder, lstm_decoder],model_prefix, epoch, net, mod.get_params()[0], mod.get_params()[1])
            print("Saved Checkpoint for Epoch", epoch)
        val_iter.reset()
        if (epoch % evaluate_val_count == 0):
            '''
            Oder macht man jetzt:
                preds = [pNTC.reshape((batch_size*tmax,len(target_dict_len))) for pNTC in mod.get_outputs()]
                labels = [lNT.reshape((batch_size * tmax,)) for lNT in batch.label]
                metric_acc.update(labels, preds)

                Kann man so auch Perplexity updaten?
            '''
            metric_acc.reset()
            metric_perp.reset()
            old_perp = new_perp
            for val_batch in val_iter:
                lstm_encoder.reset()
                lstm_decoder.reset()
                mod.forward(val_batch, is_train=False)
                preds = [pNTC.reshape((batchsize*int(tmax),target_dict_len)) for pNTC in mod.get_outputs()]
                labels = [lNT.reshape((batchsize*int(tmax),)) for lNT in val_batch.label]
                metric_acc.update(labels, preds)
                metric_perp.update(labels, preds)
            new_perp = metric_perp.get()[1]
            print('Epoch %d, Dev: %s %s' % (epoch, metric_acc.get(), new_perp))

            if (epoch>1) and hl and (abs(1-(old_perp/new_perp)) < learnratetest):
                start_learning_rate = start_learning_rate/1.3
                learnratetest = learnratetest/1.5
                mod.init_optimizer(optimizer_params=(('learning_rate',start_learning_rate), ),force_init=True)
                print("Changed learnrate")
                hl = False
            
        epoch = epoch + 1






