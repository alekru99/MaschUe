import numpy as np
import mxnet as mx
from operator import itemgetter

BATCHSIZE=1

def translate_File(beamsize, checkpointind, networkpath, source_file, isCpu, target_dict, source_dict_len, target_dict_len, encoder_dim, decoder_dim, tmax):
    model_prefix = networkpath+"/defaulttrainedrnn"
    groesse_vector_source = int(source_dict_len/100)
    groesse_vector_target = int(target_dict_len/100)
    
    #Build Encoder Mod with bind
    encoder_source = mx.sym.Variable('input_enc')
    #BATCHSIZE x tmax
    source_length = mx.sym.Variable('input_length')
    #BATCHSIZE x 1
    embedding_encoder = mx.sym.Embedding(data = encoder_source, name="embedding_enc", input_dim = source_dict_len, output_dim = groesse_vector_source)
    #BATCHSIZE x tmax x groesse_vector_source

    lstm_encoder = mx.rnn.LSTMCell(num_hidden = encoder_dim, prefix="lstm-encoder_")
    outputs_encoder, states_encoder = lstm_encoder.unroll(length = tmax, inputs = embedding_encoder, layout = 'NTC', merge_outputs = True)
    #BATCHSIZE x tmax x encoder_dim
    outputs_encoder = mx.sym.swapaxes(outputs_encoder, 0, 1)
    #tmax x BATCHSIZE x encoder_dim
    encoded_sentence = mx.sym.SequenceLast(outputs_encoder, source_length, use_sequence_length=True)
    #BATCHSIZE x encoder_dim
    encoded_sentence = mx.sym.reshape(encoded_sentence, (BATCHSIZE,1,encoder_dim))
    #BATCHSIZE x 1 x encoder_dim
    if(isCpu):
        encoder_mod = mx.mod.Module(symbol=encoded_sentence, context=mx.cpu(), data_names=['input_enc', 'input_length'],label_names=None)
    else:
        encoder_mod = mx.mod.Module(symbol=encoded_sentence, context=mx.gpu(), data_names=['input_enc', 'input_length'],label_names=None)
    encoder_mod.bind(data_shapes=[('input_enc',(BATCHSIZE,tmax)),('input_length',(BATCHSIZE,))])
    

    #Build Decoder Mod with bind
    input_decoder = mx.sym.Variable('input_dec')
    #BATCHSIZE x 1
    output_encoder = mx.sym.Variable('out_enc')
    #BATCHSIZE x 1 x encoder_dim
    in_decoder_states_h = mx.sym.Variable('states_h')
    in_decoder_states_c = mx.sym.Variable('states_c')
    #output_lstm = mx.sym.Variable('out_dec')
    #?
    embedding_decoder = mx.sym.Embedding(data = input_decoder, name="embedding_dec", input_dim = target_dict_len, output_dim = groesse_vector_target)
    #BATCHSIZE x 1 x groesse_vector_target
    pre_lstm_decoder = mx.sym.concat(embedding_decoder, output_encoder, dim = 2)
    #BATCHSIZE x 1 x encoder_dim + groesse_vector_target
    lstm_decoder = mx.rnn.LSTMCell(num_hidden = decoder_dim, prefix="lstm-decoder_")
    
    outputs_dec_lstm, states_decoder = lstm_decoder(inputs = pre_lstm_decoder, states = [in_decoder_states_h, in_decoder_states_c])

    out_states_decoder_h = states_decoder[0]
    out_states_decoder_c = states_decoder[1]
    #BATCHSIZE x 1 x decoder_dim
    outputs_decoder = mx.sym.FullyConnected(outputs_dec_lstm, name='projektionslayer', num_hidden = target_dict_len, flatten = False)
    #BATCHSIZE x 1 x target_dict_len
    outputs_decoder = mx.sym.softmax(outputs_decoder, name = 'softmax')
    #BATCHSIZE x 1 x target_dict_len
    outputs_decoder = mx.sym.Group([out_states_decoder_h, out_states_decoder_c, outputs_decoder])
    if(isCpu):
        decoder_mod = mx.mod.Module(symbol=outputs_decoder, context=mx.cpu(), data_names=['input_dec', 'out_enc', 'states_h', 'states_c'],label_names=None)
    else:
        decoder_mod = mx.mod.Module(symbol=outputs_decoder, context=mx.gpu(), data_names=['input_dec', 'out_enc', 'states_h', 'states_c'],label_names=None)
    decoder_mod.bind(data_shapes=[('input_dec',(BATCHSIZE,1)), ('out_enc',(BATCHSIZE,1,decoder_dim)), ('states_h',(BATCHSIZE,decoder_dim)), ('states_c',(BATCHSIZE,decoder_dim))])


    #Load Parameters
    _, arg_params, aux_params = mx.rnn.load_rnn_checkpoint([lstm_encoder,lstm_decoder],model_prefix,checkpointind)

    encoder_mod.set_params(arg_params, aux_params, allow_extra=True)

    decoder_mod.set_params(arg_params, aux_params, allow_extra=True)

    #Use beam_Search_Line for each Line of given File
    translation = []
    counting = 0
    for source_sentence in source_file:
        source_sentence_ext = mx.nd.zeros((BATCHSIZE,tmax))
        source_sentence_ext[0][0]= target_dict['</s>']
        source_sentence_length = 1
        for sentence_index in range(len(source_sentence)):
            source_sentence_ext[0][sentence_index+1] = source_sentence[-(sentence_index+1)]
            source_sentence_length +=1
        source_sentence_length_ext = mx.nd.zeros((BATCHSIZE,))
        source_sentence_length_ext[0][0] = source_sentence_length
        dec_begin_states = []
        dec_begin_states.append(mx.nd.zeros((BATCHSIZE,decoder_dim)))
        dec_begin_states.append(mx.nd.zeros((BATCHSIZE,decoder_dim)))
        translation.append(beam_Search_Line(beamsize, source_sentence_ext, source_sentence_length_ext, encoder_mod, decoder_mod, target_dict, dec_begin_states))
        counting+=1
        #print(counting,"/",len(source_file))
    return translation





def greedy_Search_Line(source_sentence_ext, source_sentence_length_ext, encoder_mod, decoder_mod, target_dict, dec_begin_states):
    data_encoder = [source_sentence_ext, source_sentence_length_ext]
    batch_encoder = mx.io.DataBatch(data_encoder)
    encoder_mod.forward(batch_encoder, is_train=False)
    sentence_representation = encoder_mod.get_outputs()[0]
    output_group = dec_begin_states
    target_sentence = [target_dict['<s>']]
    word_counter = 0

    while(not target_sentence[-1] == target_dict['</s>'] and not word_counter == 3 * source_sentence_length_ext[0][0]):
        last_translated_word_index = mx.nd.zeros((BATCHSIZE, 1))
        last_translated_word_index[0][0] = target_sentence[-1]
        states_h = output_group[0]
        states_c = output_group[1]
        data_decoder = [last_translated_word_index, sentence_representation, states_h, states_c]
        batch_decoder = mx.io.DataBatch(data_decoder)
        decoder_mod.forward(batch_decoder, is_train=False)
        output_group = decoder_mod.get_outputs()
        softmax_output = output_group[2]
        last_translated_word_index[0][0] = np.argmax(softmax_output[0].asnumpy())
        target_sentence.append(int(last_translated_word_index.asnumpy().item(0)))
        word_counter = word_counter + 1
    return target_sentence





def beam_Search_Line(beamsize, source_sentence_ext, source_sentence_length_ext, encoder_mod, decoder_mod, target_dict, dec_begin_states):
    data_encoder = [source_sentence_ext, source_sentence_length_ext]
    batch_encoder = mx.io.DataBatch(data_encoder)
    encoder_mod.forward(batch_encoder, is_train=False)
    sentence_representation = encoder_mod.get_outputs()[0]
    output_group = dec_begin_states
    first_translated_word_index = mx.nd.zeros((BATCHSIZE, 1))
    first_translated_word_index[0][0] = target_dict['<s>']
    states_h = output_group[0]
    states_c = output_group[1]
    #1 mal decoder
    data_decoder = [first_translated_word_index, sentence_representation, states_h, states_c]
    batch_decoder = mx.io.DataBatch(data_decoder)
    decoder_mod.forward(batch_decoder, is_train=False)
    output_group = decoder_mod.get_outputs()
    distribution = output_group[2][0].asnumpy()
    distribution_sorted = np.argsort(distribution)
    distribution_sorted = distribution_sorted[::-1]
    #append to tupel_prob_list: is list of touples (List of WordIndices, Propability of sentence normalized over length, states_h, states_c)
    tupel_prob_list = [([int(distribution_sorted[k])], distribution[distribution_sorted[k]], output_group[0], output_group[1]) for k in range(beamsize)]
    translated_words_number = 1
    while translated_words_number < 3*source_sentence_length_ext[0][0]:
        temp_tupel_prob_list = []
        #print(beamsize, len(tupel_prob_list))
        for i in range(beamsize):
            if(tupel_prob_list[i][0][-1]!=target_dict['</s>']):
                last_translated_word = tupel_prob_list[i][0][-1]
                decoder_input = mx.nd.zeros((BATCHSIZE,1))
                decoder_input[0][0] = last_translated_word
                states_h = tupel_prob_list[i][2]
                states_c = tupel_prob_list[i][3]
                data_dec = [decoder_input, sentence_representation, states_h, states_c]
                batch_decoder = mx.io.DataBatch(data_dec)
                decoder_mod.forward(batch_decoder, is_train=False)
                output_group_dec = decoder_mod.get_outputs()
                distribution = output_group_dec[2][0].asnumpy()
                distribution_sorted = np.argsort(distribution)
                distribution_sorted = distribution_sorted[::-1]
                for j in range(beamsize):
                    tempindexlist = list(tupel_prob_list[i][0])
                    tempindexlist.append(int(distribution_sorted[j]))
                    
                    temp_tupel_prob_list.append((list(tempindexlist), ((tupel_prob_list[i][1]**translated_words_number)*distribution[distribution_sorted[j]])**(1/(translated_words_number+1)),output_group_dec[0],output_group_dec[1]))
            else:
                temp_tupel_prob_list.append(tupel_prob_list[i])
                #temp_prob = ((tupel_prob_list[i][1]**translated_words_number)*distribution[target_dict['</s>']])**(1/(translated_words_number+1))
                #tempindexlist = list(tupel_prob_list[i][0])
                #tempindexlist.append(target_dict['</s>'])
                #tupel_prob_list[i] = (list(tempindexlist), temp_prob, tupel_prob_list[i][2], tupel_prob_list[i][3])
        tupel_prob_list = sorted(temp_tupel_prob_list,key=itemgetter(1))
        #print(tupel_prob_list[i][0])
        tupel_prob_list = tupel_prob_list[::-1]
        #print(tupel_prob_list[i][0])
        tupel_prob_list = tupel_prob_list[:beamsize]
        translated_words_number += 1
    return tupel_prob_list[0][0]
