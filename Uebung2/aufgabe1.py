import gzip
import argparse
import bpe

TRAIN = False
APPLY = True


text = [] #bekommt alle Zeilen der Eingabedateien übergeben
parser = argparse.ArgumentParser()
parser.add_argument('-e',"--eingabe", help="Hier soll der Ausgangskorpus angegeben werden", type=str, required=True, nargs='+')
parser.add_argument("--nonzipped", help="Hier soll angegeben werden ob das Dokument nicht gezipped ist (default ist zipped)", dest='zipped',action='store_false')
parser.set_defaults(zipped=True)
parser.add_argument('-sz', "--subwortzerlegungen", help="Anzahl der Subwort-Zerlegung Operationen", type=int)
parser.set_defaults(subwortzerlegungen=1000)
parser.add_argument("--apply", help="Hier soll angegeben werden ob die Zerlegung angewendet wird (default ist trainieren)", dest='applied',action='store_true')
parser.set_defaults(applied=TRAIN)
parser.add_argument("--casesensitive", help="Hier soll angegeben werden ob Großschreibung bei der Applikation beachtet werden soll (default ist nicht beachten)", dest='upper',action='store_true')
parser.set_defaults(upper=False)
parser.add_argument('-pf', "--picklefile", help="Hier soll die Trainingsergebnis-Datei angegeben werden", type=str)
parser.set_defaults(picklefile="traineddata.txt")
parser.add_argument("--undone", help="Hier soll angegeben werden ob der nach der Applikation wieder in Standardform übertragen werden soll (default ist nein)", dest='undone',action='store_true')
parser.set_defaults(undone=False)
args = parser.parse_args()
for argument in args.eingabe:   #Für jede übergebene Datei
    if(args.zipped):
        toappend = gzip.open(argument, 'rt', encoding="utf-8")
    else:
        toappend = open(argument, 'rt', encoding="utf-8")
    for line in toappend:
        text.append(line) #aktuelle Zeile zu text anfügen


if(args.applied == TRAIN):
    bpe.train(text, args.subwortzerlegungen, args.picklefile)
else:
    applied = bpe.apply(text,args.upper, args.picklefile)
    print(applied)
    if(args.undone):
        print(bpe.undo(applied))
    

