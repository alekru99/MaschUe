import pickle
import string


#Bekommt Liste von Wörtern mit @@'s und gibt eine Liste aller pairs mit dopplung zurück z.B.: [h@@a@@l@@l@@o]-->[h@@a@@,a@@l@@,l@@l@@,l@@o]
def findAllPairs(wordList):
    pairs = []
    for word in wordList: #Iteriere über Wörter
        symbols = word.split("@@") #Erstelle symbolliste "h@@a@@l@@l@@o"-->[h,a,l,l,o]
        symbollength = len(symbols) #symbollänge vorher berechnen um Zeit zu sparen
        if(symbollength>1): #Wörter mit einem Zeichen können nicht zusammengefasst werden
            for symbolIndex in range(0,symbollength-2): #Für alle Paare x@@y@@
                pairs.append(symbols[symbolIndex]+'@@'+symbols[symbolIndex+1]+'@@')
            pairs.append(symbols[symbollength-2]+'@@'+symbols[symbollength-1]) #Für das Paar y@@z
    return pairs


#Bekommt Text, Anzahl Operationen und Dateiname übergeben und speichert in jene Datei die BPE-Zerlegung in Form einer List nach Reihenfolge
def train(text, sz, pickledumpName):
    allWords = []
    mostCommonPairList = []
    for line in text:
            words = splitToAtAt(line).split(" ")  #Zeile -> @@ einfügen -> Wortliste
            for word in words:
                allWords.append(word) #Wordliste erstellen

    for n in range(0,sz):
        pairs = findAllPairs(allWords)             
        mydict = {}
        counter, item = 0, ''     
        for pair in pairs:                    #Finds most common Element in O(n) instead of O(n²)
            mydict[pair] = mydict.get(pair,0) + 1
            if mydict[pair] >= counter:
                counter, item = mydict[pair], pair #über pairs Iterieren und das aktuelle maximum sofort im Auge behalten
        mostCommonPair = item
        mostCommonPairAfter = mostCommonPair.replace("@@","",1) #mostCommonPair nach erstem zusammenzug (nur erstes @@ entfernen)
        mostCommonPairList.append((mostCommonPair,mostCommonPairAfter)) #Liste mit Elementen wie (a@@b@@, ab@@)
        for allWordsIndex in range(0, len(allWords)): #Wendet gerade gefundenen pair an
            allWords[allWordsIndex] = allWords[allWordsIndex].replace(str(mostCommonPair),str(mostCommonPairAfter))
    with open(pickledumpName, 'wb') as f:
        pickle.dump(mostCommonPairList, f) #Abspeichern der Zerlegung
                

#Bekommt text, Option (Großschreibung nicht entfernen) und Dateiname und gibt text mit Zerlegung aus Dateiname angewannt zurück
def apply(text, upper, pickledumpName):
    allWords = []
    with open(pickledumpName, 'rb') as f:
        mostCommonPairList = pickle.load(f) #Zerlegung öffnen
    for line in text:
            words = splitToAtAt(line).split(" ")
            for word in words:
                allWords.append(word) #Analog zu train eine Wortliste mit @@ erstellen
    #a0-a7 Speichert für jedes Wort 2=komplett Groß, 1=Erster Buchstabe Groß, 0=klein wenn gewählt
    appliedtext = []
    if(upper):
        upperpairList = [] #a0
        for allWordsIndex in range(0, len(allWords)): #a1
            if allWords[allWordsIndex].upper() == allWords[allWordsIndex]: #a2
                upperpairList.append(2) #a3
            elif list(allWords[allWordsIndex])[0].upper() == list(allWords[allWordsIndex])[0]: #a4
                upperpairList.append(1) #a5
            else: #a6
                upperpairList.append(0) #a7
    for allWordsIndex in range(0, len(allWords)):
        allWords[allWordsIndex] = allWords[allWordsIndex].lower() #(a8) #alles klein machen
    #Wirkliches Apply: Wende common Pairs auf Text an
    for commonPairApplication in mostCommonPairList:
        for allWordsIndex in range(0, len(allWords)):
            allWords[allWordsIndex] = allWords[allWordsIndex].replace(commonPairApplication[0],commonPairApplication[1])
    #a9-a15 Wiederherstellen der Großschreibung wenn gewählt
    if(upper):
        for allWordsIndex in range(0, len(allWords)): #a9
            if upperpairList[allWordsIndex]==0: #a10
                dummy=0 #a11
            elif upperpairList[allWordsIndex]==2: #a12
                allWords[allWordsIndex] = allWords[allWordsIndex].upper() #a13
            else: #a14
                allWords[allWordsIndex] = string.capwords(allWords[allWordsIndex]) #a15 macht ersten Buchstaben groß
    #Formatierung für Ausgabe        
    for allWordsIndex in range(0, len(allWords)):
        if(allWords[allWordsIndex]!='\n'):
            allWords[allWordsIndex]=allWords[allWordsIndex]+' '
    appliedtext = ''.join(allWords)
    appliedtext = appliedtext.replace('@@', '@@ ')
    return appliedtext



#Entfernt alle '@@ ' und macht bpe rückggängig
def undo(notToEditText):
    undone = notToEditText.replace('@@ ','')
    return undone


#Fügt @@'s zwischen die Zeichen von Wörtern ein, aus "Halbe Banane\n"-->"H@@a@@l@@b@@e B@@a@@n@@a@@n@@e\n"
def splitToAtAt(line):  #line is of type string
    letterList = list(line)
    for letterIndex in range(0,len(letterList)-2): #lässt \n und letztes Zeichen aus, da dort sowieso keine @@ hingehören
        if(letterList[letterIndex]!=' ' and letterList[letterIndex+1]!=' '): #Wenn nicht Leerzeichen oder Ende des Wortes
            letterList[letterIndex]=letterList[letterIndex]+"@@"
    return ''.join(letterList) #return wieder als string

