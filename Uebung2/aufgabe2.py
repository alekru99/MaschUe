import argparse
import dictonary
import pickle
import gzip

parser = argparse.ArgumentParser()
parser.add_argument("-e", "--eingabe", help="Eingabedatei für Training bzw. Anwendung.", required=True)
parser.add_argument("-nonzipped", help="Hier soll angegeben werden ob das Dokument nicht gezipped ist (default ist zipped)", dest='zipped',action='store_false')
parser.set_defaults(zipped=True)
parser.add_argument("-vName", "--vocabularyName", help="Name des zu erstelltenden Vokabular", type=str)
parser.set_defaults(vocabularyName="vocabulary.pkl")
parser.add_argument("--apply", help="Hier soll angegeben werden ob die Zerlegung angewendet wird (default ist trainieren)", dest='applied',action='store_true')
parser.set_defaults(applied=False)
args = parser.parse_args()


if not args.applied:
    # Vokabular erstellen
    if(args.zipped):
        train_data = gzip.open(args.eingabe, 'rt', encoding="utf-8")
    else:
        train_data = open(args.eingabe, 'rt', encoding="utf-8")

    dictonary.Wrapper.train(train_data, args.vocabularyName)
    
    train_data.close()
else:
    with open(args.vocabularyName, 'rb') as f:
        d = pickle.load(f)
    #Vokabular auf neuen Datensatz anwenden
    if(args.zipped):
        daten = gzip.open(args.eingabe, 'rt', encoding="utf-8")
    else:
        daten = open(args.eingabe, 'rt', encoding="utf-8")
    
    result = dictonary.Wrapper.apply(daten, d)
    print(result)
    daten.close()
