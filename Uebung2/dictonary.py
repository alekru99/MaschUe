import pickle

class Dictonary(dict): # Dictonary erbt von der Klasse dict die in Python schon vorhanden ist

    def __len__(self): # ueberschreibe len Funktion und gebe die Haelfte zurueck
        return int(dict.__len__(self) / 2)

    # ueberschreibe setitem Funktion und erstelle für Key ein Eintrag Value 
    # und fuer Value einen Eintrag Key.
    def __setitem__(self, key, value): 
        dict.__setitem__(self, key, value)
        dict.__setitem__(self, value, key)
    
    # ueberschreibe del Funktion
    # loesche beide Eintraege im Dictonary
    def __delitem__(self, key):
        value = self[key]
        dict.__delitem__(self, key)
        dict.__delitem__(self, value)
        
    def __getitem__(self, key):
        if key in self:
            return dict.__getitem__(self, key)
        else: # falls key nicht vorhanden ist gib 0 zurueck 
            #wenn key ein string sonst unbekannt Symbol
            if type(key) is str:
                return 0
            else:
                return "<UNK>"

class Wrapper:
    def train(train_data, vocabularyName):
        d = Dictonary()
        d['<s>'] = 1
        d['</s>'] = 2
        word_index = 3
        # fuer jedes wort in train_data erstelle dictonary eintrag
        for word in ''.join(train_data).split():
            # wenn Wort in Vokabular schon vorhanden mach nichts, sonst fuege hinzu
            if not word in d:
                d[word] = word_index
                word_index += 1

        # abspeichern
        pickle.dump(d, open(vocabularyName, "wb"))

    def apply(daten, d):
        
        result = ""
        for word in ''.join(daten).split():
            if word in d:
                result = result + " " + word
            else:
                result = result + " <UNK>"
        return result.strip()

