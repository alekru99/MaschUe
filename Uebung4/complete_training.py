import argparse
import batch
import pickle
import gzip
import bpe
import dictonary
import neuralnetwork
import os
import uebersetzung
import Bleuclass
import scoring
import mxnet as mx

def f_alignment(index, words_e_len, words_f_len):
    return int((float(index) / words_e_len) * words_f_len)

parser = argparse.ArgumentParser()
parser.add_argument("-tds", "--strainingsdaten", help="zipped File of Sentences to train on Source language", dest="strain_path")
parser.set_defaults(strain_path="multi30k.de.gz")
parser.add_argument("-eds", "--sentwicklungsdaten", help="File of Sentences to test neural network on Source language", dest="scheck_path")
parser.set_defaults(scheck_path="multi30k.dev.de")
parser.add_argument("-tdt", "--ttrainingsdaten", help="zipped File of Sentences to train on Target language", dest="ttrain_path")
parser.set_defaults(ttrain_path="multi30k.en.gz")
parser.add_argument("-edt", "--tentwicklungsdaten", help="File of Sentences to test neural network on Target language", dest="tcheck_path")
parser.set_defaults(tcheck_path="multi30k.dev.en")
parser.add_argument("-mp", "--modelpath", help="Ordner, der das Modell enthält", type=str)
parser.set_defaults(modelpath="defaulmodel")
#Parameters
parser.add_argument("-sz", "--subwortzerlegungen", help="Anzahl der Subwort-Zerlegung Operationen", type=int)
parser.add_argument("-bs", "--batchsize", help="Batchgrösse", type=int)
parser.add_argument("-ws", "--windowsize", help="Windowgrösse", type=int)
parser.add_argument("-lc", "--layercount", help="Layer Anzahl", type=int)
parser.add_argument("-nc", "--neurocount", help="Neuronen pro Layer", type=int)
parser.add_argument("-cd", "--checkdist", help="Anzahl der Durchläufe bis zum speichern eines Checkpoints", type=int)
parser.add_argument("-ed", "--evaldist", help="Anzahl der Durchläufe bis zum testen auf den dev Daten", type=int)
parser.add_argument("-ci", "--checkind", help="Index des zu ladenden Checkpoints", type=int)
parser.add_argument("-lr", "--learnrate", help="Lern Rate", type=float)
parser.add_argument("-af", "--activationfunction", help="Aktivierungsfunktion", type=str)
parser.add_argument("-op", "--optimizer", help="Optimizer", type=str)
parser.add_argument("-hlt", "--halflearnthreashold", help="Schwellenwert für halbierung der Lernrate", type=float)
parser.add_argument("-sm", "--searchmethod", help="0 für greedy, 1 für beam, 2 für batchbeam", type=int)
parser.add_argument("-os", "--outsize", help="Beamsize", type=int)
parser.add_argument("-tp", "--translationpath", help="Pfad des zu uebersetzenden Texts", type=str)
parser.add_argument("-tgp", "--translationgoalpath", help="Pfad des uebersetzten Texts", type=str)
parser.add_argument("-N", "--bleun", help="N für Bleu", type=int)
parser.add_argument("-ntpr", "--ntopresult", help="Anzahl der ausgegebenen Übersetzungen.", type=int)
parser.add_argument("-mnt", "--maxneurotrain", help="Anzahl der maximal auszuführenden Epochen.", type=int)
#Execution selection
parser.add_argument("-nb", "--nobpe", help="Hier soll angegeben werden ob BPE nicht benötigt ist dann soll aber eine bpe Datei angegeben werden", dest="bpee",action="store_false")
parser.set_defaults(bpee=True)
parser.add_argument("-nbap", "--nobpeapply", help="Hier soll angegeben werden ob BPE apply nicht benötigt ist dann soll aber eine bpe apply Datei angegeben werden", dest="bpeapply",action="store_false")
parser.set_defaults(bpeapply=True)
parser.add_argument("-nd", "--nodict", help="Hier soll angegeben werden ob dict erstellung nicht benötigt ist dann soll aber eine dict Datei angegeben werden", dest="dic",action="store_false")
parser.set_defaults(dic=True)
parser.add_argument("-nba", "--nobatch", help="Hier soll angegeben werden ob batch erstellung nicht benötigt ist dann soll aber eine batch Datei angegeben werden", dest="batch",action="store_false")
parser.set_defaults(batch=True)
parser.add_argument("-nt", "--notraining", help="Hier soll angegeben werden ob Training Daten nicht verarbeitet werden sollen", dest="train",action="store_false")
parser.set_defaults(train=True)
parser.add_argument("-nch", "--nocheck", help="Hier soll angegeben werden ob Check Daten nicht verarbeitet werden sollen", dest="check",action="store_false")
parser.set_defaults(check=True)
parser.add_argument("-nn", "--noneuro", help="Hier soll angegeben werden ob NEur. Netz nicht trainiert werden sollen", dest="neuro",action="store_false")
parser.set_defaults(neuro=True)
parser.add_argument("-gpu", "--gpu", help="Hier soll angegeben werden ob auf gpu trainiert werden soll", dest="cpu",action="store_false")
parser.set_defaults(cpu=True)
parser.add_argument("-load", "--load", help="Hier soll angegeben werden ob checkpoint geladen werden soll", dest="load",action="store_true")
parser.set_defaults(load=False)
parser.add_argument("-hl", "--halflearning", help="Hier soll angegeben werden ob Lernratenhalbierung benutzt werden soll", dest="halflearning",action="store_true")
parser.set_defaults(halflearning=False)
parser.add_argument("-ns", "--noscore", help="Hier soll angegeben werden ob score NICHT berechnet werden soll", dest="score",action="store_false")
parser.set_defaults(score=True)
parser.add_argument("-ntrl", "--notranslation", help="Hier soll angegeben werden ob NICHT übersetzt werden soll", dest="translation",action="store_false")
parser.set_defaults(translation=True)
parser.add_argument("-nbl", "--nobleu", help="Hier soll angegeben werden ob NICHT BLEU berechnet werden soll", dest="bleu",action="store_false")
parser.set_defaults(bleu=True)
args = parser.parse_args()

parameters = []
if not os.path.exists(os.path.dirname(args.modelpath)):
    os.makedirs(args.modelpath, exist_ok=True)
if os.path.isfile(args.modelpath+"/para"):
    for line in open(args.modelpath+"/para","r"):   
        parameters.append(line.strip('\n'))
#defaults
else:
    parameters = ["15000","200","2","3","770","2","1","10","0.1","tanh","adadelta","0.04","0","10","multi30k.dev.de","multi30k.dev.en","4","10"]

if args.subwortzerlegungen is not None:
    parameters[0]=str(args.subwortzerlegungen)
if args.batchsize is not None:
    parameters[1]=str(args.batchsize)
if args.windowsize is not None:
    parameters[2]=str(args.windowsize)
if args.layercount is not None:
    parameters[3]=str(args.layercount)
if args.neurocount is not None:
    parameters[4]=str(args.neurocount)
if args.checkdist is not None:
    parameters[5]=str(args.checkdist)
if args.evaldist is not None:
    parameters[6]=str(args.evaldist)
if args.checkind is not None:
    parameters[7]=str(args.checkind)
if args.learnrate is not None:
    parameters[8]=str(args.learnrate)
if args.activationfunction is not None:
    parameters[9]=str(args.activationfunction)
if args.optimizer is not None:
    parameters[10]=str(args.optimizer)
if args.halflearnthreashold is not None:
    parameters[11]=str(args.halflearnthreashold)
if args.searchmethod is not None:
    parameters[12]=str(args.searchmethod)
if args.outsize is not None:
    parameters[13]=str(args.outsize)
if args.translationpath is not None:
    parameters[14]=str(args.translationpath)
if args.translationgoalpath is not None:
    parameters[15]=str(args.translationgoalpath)
if args.bleun is not None:
    parameters[16]=str(args.bleun)
if args.maxneurotrain is not None:
    parameters[17]=str(args.maxneurotrain)






parstr=""
for el in parameters:
    parstr=parstr+el+"\n"
parapath = args.modelpath+"/para"
with open(parapath,"w") as f:
    f.write(parstr)

def checklist():
    filelist = os.listdir(args.modelpath)
    filterlist = [int(k[17:21]) for k in filelist if ".params" in k]
    return (filterlist)
def translate(checkind,searchmethod):
    translated = []
    mod = mx.mod.Module.load(args.modelpath+"/defaulttrainednn", checkind, False, data_names=\
        ['train_source_window','train_target_window'], label_names=['softmax_label'])
    mod.bind([('train_source_window',(1,2*int(parameters[2])+1)),('train_target_window',(1,int(parameters[2])))])

    dic_sc = open(args.modelpath+"/defaultdict.de", "rb") 
    dictsc = pickle.load(dic_sc)
    dic_tc = open(args.modelpath+"/defaultdict.en", "rb")
    dicttc = pickle.load(dic_tc) 
    for line in open(parameters[14]):
        line=bpe.apply([line],False,args.modelpath+"/a1trained7k.deen")
        
        line = line.strip("\n")
        linelist = line.split(" ")
        inputlist = []
        for word in linelist:
            inputlist.append(dictsc[word])
        inputlist.append(2)
        output = uebersetzung.uebersetzen(searchmethod,int(parameters[13]),int(checkind),args.modelpath+"/defaulttrainednn",inputlist,mod) 
        #print(parameters[12],parameters[13],checkind,args.modelpath+"/defaulttrainednn",inputlist,output)
        output = [ind for ind in output if ind!=1 and ind!=2]
        transstr=[]
        for ind in output:
            transstr.append(dicttc[ind])
        translated.append(" ".join(transstr))
    return(bpe.undo("\n".join(translated)))
#translated ist translate mit print
def translated(checkind):
    translated = []
    mod = mx.mod.Module.load(args.modelpath+"/defaulttrainednn", checkind, False, data_names=\
        ['train_source_window','train_target_window'], label_names=None)
    mod.bind([('train_source_window',(1,2*int(parameters[2])+1)),('train_target_window',(1,int(parameters[2])))])
    dic_sc = open(args.modelpath+"/defaultdict.de", "rb") 
    dictsc = pickle.load(dic_sc)
    dic_tc = open(args.modelpath+"/defaultdict.en", "rb")
    dicttc = pickle.load(dic_tc) 
    for line in open(parameters[14]): #was zu übersetzrn ist
        #print(line)
        line=bpe.apply([line],False,args.modelpath+"/a1trained7k.deen")
        #print(line)
        line = line.strip("\n")
        linelist = line.split(" ")
        inputlist = []
        for word in linelist:
            inputlist.append(dictsc[word])
        inputlist.append(2)
        output = uebersetzung.uebersetzen(2,int(parameters[13]),int(checkind),args.modelpath+"/defaulttrainednn",inputlist, mod)  #12 searchmethod (0 greedy,1 beam,2 batchbeam) 13 beamsize
        #print(parameters[12],parameters[13],checkind,args.modelpath+"/defaulttrainednn",inputlist,output)
        #print(output)
        output = [ind for ind in output if ind!=1 and ind!=2]
        transstr=[]
        for ind in output:
            transstr.append(dicttc[ind])
        translated.append(" ".join(transstr))
        #print("\n".join(translated),"\n")
        print(bpe.undo("\n".join(translated)))
        translated = []







traindatas = gzip.open(args.strain_path, "rt")
traindatat = gzip.open(args.ttrain_path, "rt")
checkdatas = open(args.scheck_path, "rt")
checkdatat = open(args.tcheck_path, "rt")

if(args.train):
    print("\nTrainingdata:")
    texts = []
    textt = []
    for line in traindatas:
        texts.append(line)
    for line in traindatat:
        textt.append(line)
    text = texts + textt
    

    if(args.bpee):
        bpe.train(text, int(parameters[0]), args.modelpath+"/a1trained7k.deen")
  
    bpedats = open(args.modelpath+"/a1trained7k.deen", "rb")
    bpedatt = open(args.modelpath+"/a1trained7k.deen", "rb")  
    bpedatas = pickle.load(bpedats)
    bpedatat = pickle.load(bpedatt)

    if(args.bpeapply):
        print("\nBPE Source Application:")
        applieds = bpe.apply(texts,False, args.modelpath+"/a1trained7k.deen")
        with open(args.modelpath+"/defaultbpeapply.de", "wt") as bpeapplieds:
            print(applieds, file=bpeapplieds)
        print("\nBPE Target Application:")
        appliedt = bpe.apply(textt,False, args.modelpath+"/a1trained7k.deen")
        with open(args.modelpath+"/defaultbpeapply.en", "wt") as bpeappliedt:
            print(appliedt, file=bpeappliedt)

    bpeapps = open(args.modelpath+"/defaultbpeapply.de", "rt")
    bpeappt = open(args.modelpath+"/defaultbpeapply.en", "rt")

    if(args.dic):
        dictonary.Wraper.train(bpeapps, args.modelpath+"/defaultdict.de")
        print("\nDone making Source Dict")
        dictonary.Wraper.train(bpeappt, args.modelpath+"/defaultdict.en")
        print("\nDone making Target Dict")

    print("\nCreating Batch:")
    bpeapps = open(args.modelpath+"/defaultbpeapply.de", "rt")
    bpeappx = open(args.modelpath+"/defaultbpeapply.de", "rt")
    bpeappt = open(args.modelpath+"/defaultbpeapply.en", "rt")  
    dic_s = open(args.modelpath+"/defaultdict.de", "rb")
    dic_t = open(args.modelpath+"/defaultdict.en", "rb")  
    dicts = pickle.load(dic_s)
    dictt = pickle.load(dic_t)

    if(args.batch):
        all_batches = batch.createBatches(int(parameters[1]), int(parameters[2]), f_alignment, bpeappt, bpeapps, bpeappx, dictt, dicts)
        print("\nDone making Batch")
        pickle.dump(all_batches, open(args.modelpath+"/defaultbatch", "wb"))

if(args.check):
    print("\nCheckdata:")
    textsc = []
    texttc = []
    for line in checkdatas:
        textsc.append(line)
    for line in checkdatat:
        texttc.append(line)
    

    bpedatsc = open(args.modelpath+"/a1trained7k.deen", "rb")
    bpedattc = open(args.modelpath+"/a1trained7k.deen", "rb")  
    bpedatasc = pickle.load(bpedatsc)
    bpedatatc = pickle.load(bpedattc)

    if(args.bpeapply):
        print("\nBPE Source Application:")
        appliedsc = bpe.apply(textsc,False, args.modelpath+"/a1trained7k.deen")
        with open(args.modelpath+"/defaultcheckbpeapply.de", "wt") as bpeappliedsc:
            print(appliedsc, file=bpeappliedsc)
        print("\nBPE Target Application:")
        appliedtc = bpe.apply(texttc,False, args.modelpath+"/a1trained7k.deen")
        with open(args.modelpath+"/defaultcheckbpeapply.en", "wt") as bpeappliedtc:
            print(appliedtc, file=bpeappliedtc)

    

    print("Creating Batch:")
    bpeappsc = open(args.modelpath+"/defaultcheckbpeapply.de", "rt")
    bpeappxc = open(args.modelpath+"/defaultcheckbpeapply.de", "rt")
    bpeapptc = open(args.modelpath+"/defaultcheckbpeapply.en", "rt")  
    dic_sc = open(args.modelpath+"/defaultdict.de", "rb")
    dic_tc = open(args.modelpath+"/defaultdict.en", "rb")  
    dictsc = pickle.load(dic_sc)
    dicttc = pickle.load(dic_tc)

    if(args.batch):
        all_batches = batch.createBatches(int(parameters[1]), int(parameters[2]), f_alignment, bpeapptc, bpeappsc, bpeappxc, dicttc, dictsc)
        print("\nDone making Batch")
        pickle.dump(all_batches, open(args.modelpath+"/defaultcheckbatch", "wb"))

#print("\nFinished preparing for neural network")
if(args.neuro):
    all_batches = pickle.load(open(args.modelpath+"/defaultbatch", "rb"))
    all_cbatches = pickle.load(open(args.modelpath+"/defaultcheckbatch", "rb"))
    dic_sc = open(args.modelpath+"/defaultdict.de", "rb")
    dic_tc = open(args.modelpath+"/defaultdict.en", "rb")  
    dictsc = pickle.load(dic_sc)
    dicttc = pickle.load(dic_tc)
    if not checklist():
        cl =  0
    else:
        cl = max(checklist())
    neuralnetwork.neurotrain(all_batches, all_cbatches ,int(parameters[1]),int(parameters[3]),int(parameters[4]),len(dictsc),len(dicttc),args.cpu,1,1, False, 0, args.halflearning, float(parameters[8]), parameters[9], parameters[10], parameters[11],args.modelpath,int(parameters[17]))

#aufgabe 2
if(args.score):
    longest_sentence_length=scoring.get_length_of_longest_sentence(args.tcheck_path)
    source_file = open(args.scheck_path,"rt")
    target_file = open(args.tcheck_path,"rt")
    source_file2 = open(args.scheck_path,"rt")
    target_file2 = open(args.tcheck_path,"rt")
    dic_sc = open(args.modelpath+"/defaultdict.de", "rb")
    dic_tc = open(args.modelpath+"/defaultdict.en", "rb")  
    dict_source = pickle.load(dic_sc)
    dict_target = pickle.load(dic_tc)
    batch_list = scoring.create_batch(source_file, target_file, longest_sentence_length, dict_source, dict_target, int(parameters[2]), f_alignment)
    scoring.scoring(int(parameters[7]), args.modelpath+"/defaulttrainednn", batch_list, longest_sentence_length, source_file2, target_file2)
#aufgabe 4
if(args.translation):
    translated(max(checklist()))
#aufgabe 5
if(args.bleu):
    ref = []
    hyp = []
    for line in open(parameters[15]): #Referezübersetzung
        ref.append(line)
    for checkindex in checklist():
        hyptext=translate(checkindex,0)
        hyp=hyptext.split("\n")
        print(Bleuclass.BLEUcalc.BLEU(ref,hyp,int(parameters[16])))


