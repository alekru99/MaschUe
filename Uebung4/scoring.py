import mxnet as mx
import numpy


def get_length_of_longest_sentence(target_file_path):
    length_of_longest_sentence = 0
    with open(target_file_path, 'r', encoding="utf-8") as target_file:
        for satz in target_file:
            words = satz.split()
            words_len = len(words)
            if words_len > length_of_longest_sentence:
                length_of_longest_sentence = words_len
    return length_of_longest_sentence+1
    
def create_batch(source_file, target_file, longest_sentence_length, dict_source,\
        dict_target, w, f_allignment):
    first_runthrough = True
    batch_zeile = 0
    batches = []
    
    for satz_source, satz_target in zip(source_file, target_file):
        source_window = mx.nd.zeros((longest_sentence_length, 2*w+1))
        target_window = mx.nd.zeros((longest_sentence_length, w))
        target_label = mx.nd.zeros((longest_sentence_length, 1))
        
        words_source = satz_source.split()
        words_target = satz_target.split()
        len_words_source = len(words_source)
        len_words_target = len(words_target)
        words_target.append("</s>")
        
        for word in words_target:
            
            #Target label auffüllen
            target_label[batch_zeile][0] \
                = dict_target[word]
            
            #Target Window auffüllen
            first_target = batch_zeile - w
            for i in range(0, w):
                #prüfe, ob man vor erstem Wort des Satzes ist
                if first_target + i < 0:
                    target_window[batch_zeile][i] =\
                        dict_target["<s>"]
                else:
                    target_window[batch_zeile][i] =\
                        dict_target[words_target[first_target + i]]
                        
            #Source Window auffüllen
            #Bereche bi
            bi = f_allignment(batch_zeile, len_words_target, len_words_source)
            
            first_source = bi - w
            for i in range(0, 2 * w + 1):
                #prüfe, ob man vor dem ersten Wort des Satzes ist
                if first_source + i < 0:
                    source_window[batch_zeile][i] =\
                        dict_source["<s>"]
                #prüfe, ob man hinter dme letzten Wort des Satzes ist
                elif first_source + i >= len_words_source:
                    source_window[batch_zeile][i] =\
                        dict_source["</s>"]
                else:
                    source_window[batch_zeile][i] =\
                        dict_source[words_source[first_source + i]]
            
            batch_zeile += 1
            
        batch_zeile = 0
        
        if(first_runthrough):
            batches = [source_window, target_window, target_label]
            first_runthrough = False
        else:
            batches[0] = mx.ndarray.concat(batches[0], source_window, dim=0)
            batches[1] = mx.ndarray.concat(batches[1], target_window, dim=0)
            batches[2] = mx.ndarray.concat(batches[2], target_label, dim=0)
    
    return batches
            
                


def scoring(checkpointind, networkpath, batch_list, longest_sentence_length, source_file, target_file):
    source_window = batch_list[0]
    target_window = batch_list[1]
    target_label = batch_list[2].T[0]
    data = {'train_source_window':source_window, 'train_target_window': target_window}
    nd_iter = mx.io.NDArrayIter(data, target_label, longest_sentence_length)
    mod = mx.mod.Module.load(networkpath, checkpointind, False, data_names=['train_source_window','train_target_window'], label_names=['softmax_label'])
    mod.bind(data_shapes=nd_iter.provide_data, label_shapes=nd_iter.provide_label, force_rebind = True)
    for batch in nd_iter: 
        mod.forward(batch, is_train = False)
        distributions = mod.get_outputs()[0]
        total_probability = 1
        for i in range(0, longest_sentence_length):
            distribution = distributions[i].asnumpy()
            probability_index = (batch.label[0][i]).asnumpy() #keine Ahnung ob das klappt amk
            total_probability = total_probability * distribution[int(probability_index)]
        
        print(total_probability, source_file.readline(), target_file.readline())    
    
