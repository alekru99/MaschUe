import mxnet as mx
import pickle
import numpy


source_window = mx.nd.zeros((1, 5))
target_window = mx.nd.zeros((1, 2))


source_window[0][0] = 1
source_window[0][1] = 1
source_window[0][2] = 0 #.
source_window[0][3] = 39
source_window[0][4] = 526

target_window[0][0] = 1
target_window[0][1] = 1


dict_sc = open('defaultdict.de', "rb")
dict_tc = open('defaultdict.en', "rb")
dicttc = pickle.load(dict_tc)
dictsc = pickle.load(dict_sc)

print((source_window[0][0]).asnumpy()[0])

print(dictsc[(source_window[0][0]).asnumpy()[0]])
print(dictsc[(source_window[0][1]).asnumpy()[0]])
print(dictsc[(source_window[0][2]).asnumpy()[0]])
print(dictsc[(source_window[0][3]).asnumpy()[0]])
print(dictsc[(source_window[0][4]).asnumpy()[0]])

print(dicttc[(target_window[0][0]).asnumpy()[0]])
print(dicttc[(target_window[0][1]).asnumpy()[0]])


target_label = mx.nd.zeros((1))

data = {'train_source_window':source_window, 'train_target_window': target_window}
nd_iter = mx.io.NDArrayIter(data, target_label, 1)

#model_loaded = mx.mod.Module.load('defaulttrainednn',10)
mod = mx.mod.Module.load('defaulttrainednn', 10, False, data_names=['train_source_window','train_target_window'], label_names=['softmax_label'])
mod.bind(data_shapes=nd_iter.provide_data, label_shapes=nd_iter.provide_label)

for batch in nd_iter:
    mod.forward(batch, is_train=False)
    bla = mod.get_outputs()[0].asnumpy()
    print(bla)
    #bla = mod.predict(eval_data=nd_iter, num_batch=1)
    print(numpy.argsort(bla))
    print(dicttc[numpy.argmax(bla)])
