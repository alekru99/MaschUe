import mxnet as mx
import numpy as np

groesse_vektor = 20

def batchsep(batches):
    first_loop = True
    for batch in batches:
        if first_loop:
            source_windows = batch[0]
            target_windows = batch[1]
            target_labels = batch[2]
            first_loop = False
        else:
            source_windows = mx.ndarray.concat(source_windows, batch[0], dim=0)
            target_windows = mx.ndarray.concat(target_windows, batch[1], dim=0)
            target_labels = mx.ndarray.concat(target_labels, batch[2], dim=0)
    return((source_windows,target_windows,target_labels))


def neurotrain(train_batches, val_batches, batch_size, layercount, neurocount,source_dict_len, 
        target_dict_len,cpu,checkpoint_save_count,evaluate_val_count,isLoad,
        load_checkpoint_nr,is_reduce_learning_rate, start_learning_rate, actfun, opt, hlt,modelpath,maxtrain):
    groesse_vektor = int(source_dict_len/100)
    (train_source_window, train_target_window, train_target_label) = batchsep(train_batches)
    (val_source_window, val_target_window, val_target_label) = batchsep(val_batches)
    mx.random.seed(7777)
    train_target_label=train_target_label.T[0]
    val_target_label=val_target_label.T[0]
    data_nd = {'train_source_window' : train_source_window, 'train_target_window' : train_target_window}
    nd_iter = mx.io.NDArrayIter(data_nd, train_target_label, batch_size)
    data_val = {'val_source_window' : val_source_window, 'val_target_window' : val_target_window}
    val_iter = mx.io.NDArrayIter(data_val, val_target_label, batch_size)
    
    #Create NN
    embedding_net_source = mx.sym.Variable('train_source_window')
    embedding_net_target = mx.sym.Variable('train_target_window')
    embedding_net_source = mx.sym.Embedding(data = embedding_net_source, input_dim=source_dict_len, output_dim= groesse_vektor, name='el_source')
    embedding_net_target = mx.sym.Embedding(data = embedding_net_target, input_dim=target_dict_len, output_dim=groesse_vektor, name='el_target')
    fully_connected_left = mx.sym.FullyConnected(embedding_net_source, name='fcl', num_hidden=int(neurocount/5))
    fully_connected_left = mx.sym.Activation(fully_connected_left, name=actfun+'relul', act_type=actfun)
    fully_connected_right = mx.sym.FullyConnected(embedding_net_target, name='fcr', num_hidden=int(neurocount/10))
    fully_connected_right = mx.sym.Activation(fully_connected_right, name='relur', act_type=actfun)
    net = mx.sym.concat(fully_connected_left, fully_connected_right)
    net = mx.sym.FullyConnected(net, name='fc1', num_hidden=int(neurocount/2))
    #batch normalization
    
    for n in range(0,layercount):
        net = mx.sym.Activation(net, name='relu'+str(n), act_type=actfun)
        net = mx.sym.FullyConnected(net, name='fc'+str(n+2), num_hidden=neurocount)
        if(n==0):
            net = mx.sym.BatchNorm(net, name='batch_norm_layer1')
    #batch normalization
    #net = mx.sym.BatchNorm(net, name='batch_norm_layer2')
    net = mx.sym.Activation(net, name='relu', act_type=actfun)
    net = mx.sym.FullyConnected(net, name='projektions_layer', num_hidden=target_dict_len)
    net = mx.sym.SoftmaxOutput(net, name='softmax')
    #digraph = mx.viz.plot_network(net, save_format = 'jpg')
    #digraph.render()
    model_prefix = modelpath+'/defaulttrainednn'
    print("Graph saved")



    if(cpu):
        mod=mx.mod.Module(symbol=net, context=mx.cpu(), data_names=['train_source_window','train_target_window'], label_names=['softmax_label'])
    else:
        mod=mx.mod.Module(symbol=net, context=mx.gpu(), data_names=['train_source_window','train_target_window'], label_names=['softmax_label'])
    if (isLoad):
        mod = mx.mod.Module.load(model_prefix, load_checkpoint_nr,False, data_names=['train_source_window','train_target_window'], label_names=['softmax_label'])
        #mod.set_params(arg_params=mod.get_params()[0], aux_params=mod.get_params()[1])

    mod.bind(data_shapes=nd_iter.provide_data, label_shapes=nd_iter.provide_label)
    if(not isLoad):
        mod.init_params(initializer=mx.init.Xavier(factor_type="in", magnitude=2.34))
    mod.init_optimizer(optimizer=opt, optimizer_params=(('learning_rate',start_learning_rate), ))
    metric_acc = mx.metric.create('acc')
    metric_perp = mx.metric.Perplexity(ignore_label=None)
    learning_rate=start_learning_rate  
    epoch = 1
    new_perp = 0
    learnratetest=float(hlt)
    while(epoch<=maxtrain):
        nd_iter.reset()
        val_iter.reset()
        metric_acc.reset()
        metric_perp.reset()
        for batch in nd_iter:
            mod.forward(batch, is_train=True)
            mod.update_metric(metric_acc, batch.label)
            mod.update_metric(metric_perp, batch.label)
            mod.backward()
            mod.update()
        print('Epoch %d, Training %s %s' % (epoch, metric_acc.get(), metric_perp.get()))
        
        
        
        if (epoch % checkpoint_save_count == 0):
            mx.model.save_checkpoint(model_prefix, epoch, net, mod.get_params()[0], mod.get_params()[1] )
        val_iter.reset()
        if (epoch % evaluate_val_count == 0):
            metric_acc.reset()
            metric_perp.reset()
            score = mod.score(val_iter, ["acc",metric_perp])
            print("Accuracy score on dev is %f" % (score[0][1]))
            print("Perplexity score on dev is %f" % (score[1][1]))
            
            old_perp = new_perp
            new_perp = score[1][1]   #metric_perp.get()
            if (epoch>1) and is_reduce_learning_rate and (abs(1-(old_perp/new_perp)) < learnratetest):
                learning_rate = learning_rate/2.
                learnratetest = learnratetest/2.
                mod.init_optimizer(optimizer_params=(('learning_rate',learning_rate), ),force_init=True)
                print("Changed learnrate")
            
        epoch = epoch + 1
          
