import numpy as np
import mxnet as mx
import pickle
import math
from enum import Enum
from operator import itemgetter


windowSize = 2

class SearchMethod(Enum):
    Greedy = 0
    Beam = 1
    Batchbeam = 2

#Vokabular laden
dict_sc = open('defaultdict.de', "rb")
dict_tc = open('defaultdict.en', "rb")
dicttc = pickle.load(dict_tc)
dictsc = pickle.load(dict_sc)

def initBatchWithFirstWord(inputline):
    source_window = mx.nd.zeros((1, 2 * windowSize + 1))
    target_window = mx.nd.zeros((1, windowSize))
    target_label = mx.nd.zeros((1))

    #first word
    for ind in range(0, windowSize+1):
        #fill left target_window and left half of source_window with 1
        if not ind == windowSize:
            source_window[0][ind] = 1
            target_window[0][ind] = 1
        #fill rest of target_window with words from input line.
        if ind < len(inputline):
            source_window[0][ind + windowSize] = inputline[ind]
        #no word from input line is left, so we fill with 2
        else: 
            source_window[0][ind + windowSize] = 2

    return source_window, target_window, target_label

def uebersetzen(method, beamsize, checkpointind, networkpath, inputline, mod):
    return beam2(beamsize, checkpointind, networkpath, inputline, mod)

def beam2(beamsize, checkpointind, networkpath, inputline, mod):
    inputline_length = len(inputline)
    windowsize = 2
    source_window, target_window, target_label = initBatchWithFirstWord(inputline)
    data = [source_window, target_window]
    batch = mx.io.DataBatch(data)

    mod.forward(batch, is_train=False)

    distribution = mod.get_outputs()[0][0].asnumpy()
    distribution_sorted = np.argsort(distribution)
    distribution_sorted = distribution_sorted[::-1]
    #append to tupel_prob_list: is list of touples (List of WordIndices, Propability of sentence normalized over length)
    tupel_prob_list = [([int(distribution_sorted[k])], distribution[int(distribution_sorted[k])]) for k in range(beamsize)]
    
    translated_words_number = 1
    while translated_words_number<=inputline_length*3:
        temp_tupel_prob_list = []
        #Verschiebe Elemente in den Windows nach links
        for ind in range(0,2*windowSize):
            source_window[0][ind] = source_window[0][ind+1].asnumpy()
        #Fülle source_window mit nächstem Wort vom Satz auf, falls möglich
        if(windowSize+translated_words_number<len(inputline))  :
            source_window[0][2*windowSize] = inputline[windowSize+translated_words_number]
        else:
            source_window[0][2*windowSize] = 2

        #Für die besten beamsize vorherigen Übersetzungen
        for i in range(beamsize):
            if(tupel_prob_list[i][0][-1]!=2):
                #Target Window anpassen
                for windowindex in range (windowsize):
                    #Passe target_window an, je nach vorheriger Übersetzung
                    if((translated_words_number-windowsize+windowindex)>=0):
                        target_window[0][windowindex] = tupel_prob_list[i][0][translated_words_number-windowsize+windowindex]
                    else:
                        target_window[0][windowindex] = 1

                #Netzwerk ausführen
                data = [source_window, target_window]
                batch = mx.io.DataBatch(data)

                mod.forward(batch, is_train=False)

                distribution = mod.get_outputs()[0][0].asnumpy()
                distribution_sorted = np.argsort(distribution)
                distribution_sorted = distribution_sorted[::-1]

                for j in range(beamsize):
                    tempindexlist = list(tupel_prob_list[i][0])
                    tempindexlist.append(int(distribution_sorted[j]))

                    temp_tupel_prob_list.append((list(tempindexlist), ((tupel_prob_list[i][1]**translated_words_number)*distribution[distribution_sorted[j]])**(1/(translated_words_number+1))))
            else:
                temp_tupel_prob_list.append(tupel_prob_list[i])
        tupel_prob_list = sorted(temp_tupel_prob_list,key=itemgetter(1))
        #print(tupel_prob_list[i][0])
        tupel_prob_list = tupel_prob_list[::-1]
        #print(tupel_prob_list[i][0])
        tupel_prob_list = tupel_prob_list[:beamsize]
        translated_words_number += 1
    return tupel_prob_list[0][0]





    

