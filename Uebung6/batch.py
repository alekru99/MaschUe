import mxnet as mx
import sys



def tMax(target_file_path):
    length_of_longest_sentence = 0
    with open(target_file_path, 'r', encoding="utf-8") as target_file:
        for satz in target_file:
            words = satz.split()
            words_len = len(words)
            if words_len > length_of_longest_sentence:
                length_of_longest_sentence = words_len
    return length_of_longest_sentence+1
    

def progress(count, total):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '#' * filled_len + ' ' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s \r' % (bar, percents, '%'))
    sys.stdout.flush() 


def crBa(fileE_path, fileF_path, dictE, dictF):
    with open(fileE_path) as fileE:
        numSent = 0
        for line in fileE:
            numSent+=1
    maxSentenceLength = int((max(tMax("defaulmodel/defaultbpeapply.de"),tMax("defaulmodel/defaultbpeapply.en"))+1)*1.15)
    data = mx.nd.zeros((numSent, 2*maxSentenceLength))
    label = mx.nd.zeros((numSent, maxSentenceLength))
    source_length = mx.nd.zeros((numSent,))
    fileE = open(fileE_path)
    fileF = open(fileF_path)
    dataIndex = 0
    for satz_e, satz_f in zip(fileE, fileF):
        wordIndex = 0
        wordlist = satz_f.split()
        for i in range(len(wordlist)):

            data[dataIndex][i] = dictF[wordlist[i]]
            wordIndex=i
        data[dataIndex][len(wordlist)]=dictF['</s>']
        source_length[dataIndex] = wordIndex+1
        wordIndex = 0
        for word in satz_e.split():
            data[dataIndex][maxSentenceLength+1+wordIndex] = dictE[word]
            label[dataIndex][wordIndex] = dictE[word]
            wordIndex += 1
        data[dataIndex][maxSentenceLength]=dictE['<s>']
        label[dataIndex][wordIndex]=dictE['</s>']
        dataIndex += 1
    return data, label, source_length
        



def createBatches(batchGroesse, w, f_allignment, fileE, fileF, fileX, dictE, dictF):
    '''

    :param batchGroesse: Anzahl der Zeilen des Batches
    :param w: Fenstergröße
    :param f_allignment: Funktion, die einer Target Position eine Source Position zuordnet
    :param fileE_path: Datei, die Text in Zielsprache enthält
    :param fileF_path: Datei, die Text in Quellsprache enthält
    :param dictE: Vokabular der Zielsprache
    :param dictF: Vokabular der Quellsprache
    :return: Liste von Batches
    '''

    batch_zeile = 0
    #erstelle NDArrays mit entsprechender Größe
    source_window = mx.nd.zeros((batchGroesse, 2 * w + 1))
    target_window = mx.nd.zeros((batchGroesse, w))
    target_label = mx.nd.zeros((batchGroesse, 1))
    batches = []
    fileX
    lenprog = sum(1 for line in fileX)
    indexprog = 0       
    progress(indexprog,lenprog)
    for satz_e, satz_f in zip(fileE, fileF):
        index = 0 #gibt an, bei welchem Wort wir in satz_e sind
        words_e = satz_e.split()
        words_f = satz_f.split()
        words_e_len = len(words_e)
        words_f_len = len(words_f)
        words_e.append("</s>")
        if words_e_len==0:
            break
        for word in words_e:
            #Target label auffüllen
            target_label[batch_zeile % batchGroesse][0] = dictE[word]
            #Target_window auffüllen
            first_target = index - w
            for i in range(0, w):
                #prüfe, ob man vor erstem Wort des Satzes ist
                if first_target + i < 0:
                    target_window[batch_zeile % batchGroesse][i] = dictE["<s>"]
                else:
                    target_window[batch_zeile % batchGroesse][i] = dictE[words_e[first_target + i]]

            #Source window auffüllen
            #Berechne bi  
            bi = f_allignment(index, words_e_len, words_f_len)

            first_source = bi - w
            for i in range(0, 2 * w + 1):
                # prüfe, ob man vor erstem Wort des Satzes ist
                if first_source + i < 0:
                    source_window[batch_zeile % batchGroesse][i] = dictF["<s>"]
                # prüfe, ob man hinter dem letzten Wort des Satzes ist
                elif first_source + i >= words_f_len:
                    source_window[batch_zeile % batchGroesse][i] = dictF["</s>"]
                else:
                    source_window[batch_zeile % batchGroesse][i] = dictF[words_f[first_source + i]]

            batch_zeile += 1
            index += 1

            #Prüfe, ob Batch voll ist
            if (batch_zeile % batchGroesse == 0):
                #Füge vollen Batch der BatchListe hinzu
                current_finished_batch = [source_window, target_window, target_label]
                batches.append(current_finished_batch)
                #Neue NDArrays für die nächsten Zeilen
                source_window = mx.nd.zeros((batchGroesse, 2 * w + 1))
                target_window = mx.nd.zeros((batchGroesse, w))
                target_label = mx.nd.zeros((batchGroesse, 1))
        indexprog = indexprog + 1       
        progress(indexprog,lenprog)
    if not batch_zeile % batchGroesse == 0:
        last_batch = [source_window, target_window, target_label]
        batches.append(last_batch)
    return batches

def print_batches(list_batches, zeile_anfang, zeile_ende, batch_size, function, dictE, dictF, file_name):
    '''

    :param list_batches: Liste mit Batches
    :param zeile_anfang:
    :param zeile_ende:
    :param batch_size:
    :param function: Funktion, die eine Zeile des NDArrray als String zurückgibt
    :param d: Dictionary
    :param file_name: Datei, in die die Elemente der Batches der ausgewählten Zeilen geschrieben werden sollen
    :return:
    '''
    with open(file_name, 'wt') as file:
        first_batch = zeile_anfang // batch_size
        last_batch = zeile_ende // batch_size
        first_line_of_interest = zeile_anfang % batch_size
        last_line_of_interest = zeile_ende % batch_size
        for i in range(first_batch, last_batch + 1): #durchlaufe die relevanten Batches
            current_batch = list_batches[i]
            if first_batch == last_batch:
                for j in range(first_line_of_interest, last_line_of_interest + 1):
                    file.write(function(current_batch[0], j, dictF) + " " + \
                               function(current_batch[1], j, dictE) + " " + \
                               function(current_batch[2], j, dictE) + "\n")
            elif i == first_batch:
                for j in range(first_line_of_interest, batch_size): #durchlaufe relevanten Zeilen und schreibe sie ins file
                    file.write(function(current_batch[0], j, dictF) + " " + \
                        function(current_batch[1], j, dictE) + " " + \
                        function(current_batch[2], j, dictE) + "\n")
            elif i == last_batch:
                for j in range(0, last_line_of_interest + 1): #durchlaufe relevanten Zeilen und schreibe sie ins file
                    file.write(function(current_batch[0], j, dictF) + " " + \
                            function(current_batch[1], j, dictE) + " " + \
                            function(current_batch[2], j, dictE) + "\n")
            else:
                for j in range(0, batch_size): #durchlaufe relevanten Zeilen und schreibe sie ins file
                    file.write(function(current_batch[0], j, dictF) + " " + \
                            function(current_batch[1], j, dictE) + " " + \
                            function(current_batch[2], j, dictE) + "\n")

def getIndexStringFromNDArrayLine(ndArray, line, d):
    current_line = ndArray[line]
    result = "["
    for cell in current_line:
        #Steppe durch jede Zelle einer Zeile, wandle um in numpy array, um dann Zugriff auf das Item zu erhalten
        result += " " + str(int(cell.asnumpy().item()))
    result += "]"
    return result

def getStringFromNDArrayLine(ndArray, line, d):
    current_line = ndArray[line]
    result = "["
    for cell in current_line:
        # Steppe durch jede Zelle einer Zeile, wandle um in numpy array, um dann Zugriff auf das Item zu erhalten und
        # frage den entsprechenden String aus dem Dictionary ab
        result += " " + d[int(cell.asnumpy().item())]
    result += "]"
    return result

def print_batches_index(list_batches, zeile_anfang, zeile_ende, batch_size, dictE, dictF,outputIndex):
    #Schreibe die Einträge der relevanten Zeilen der Batches in das File batches_index.txt
    print_batches(list_batches, zeile_anfang, zeile_ende, batch_size, getIndexStringFromNDArrayLine, dictE, dictF, outputIndex)

def print_batches_str(list_batches, zeile_anfang, zeile_ende, batch_size, dictE, dictF, outputString):
    # Schreibe die zu den Einträgen(Indices) gehörenden Strings der relevanten Zeilen der Batches in das File batches_str.txt
    print_batches(list_batches, zeile_anfang, zeile_ende, batch_size, getStringFromNDArrayLine, dictE, dictF, outputString)
