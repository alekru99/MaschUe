
import math
class BLEUcalc:
    def BLEU(ref,hyp,N): #ref und hyp sind komplette Korpusse
        geso=0 #Summiert den oberen Teil des Pn Bruches auf (Summe über alle Sätze)
        gesu=0 #analog nur unterer Teil des Bruches
        gesreflen=0 #gesammte Referenzlänge über alle Zeilen
        geshyplen=0   #gesamte Hypothesenlänge über alle Zeilen
        #N=4 #int(input("Welches N soll benutzt werden?")) # Alternativcode
        X = 0 #Summe der logarithmen von Pn über N
        for index in range(1,N+1): #Iteration von 1 bis N
            for i in range(0,len(ref)): #Iteration über Sätze
                refe = ref[i].split()
                hypo = hyp[i].split()
                m=BLEUcalc.pn(list(refe),list(hypo),index) #m enthällt [rechte Summe von Pn oben,rechte Summe von Pn unten]
                geso+=m[0] #Aufsummieren über die Zeilen
                gesu+=m[1]
                if index==1:
                    gesreflen+=len(refe) #Aufsummieren der Korpuslängen nur bei N=1
                    geshyplen+=len(hypo)
            if(gesu and geso):
                X+=math.log(geso/gesu) #Aufsummieren der log von Pn (Pn=geso/gesu)
            geso=0
            gesu=0
        X=X/N    #Erst zum Schluss mal 1/N (Reihenfolge egal)
        
        BLEU=BLEUcalc.BP(geshyplen,gesreflen)*math.exp(X)
        return BLEU
        
    def BP(geshyplen,gesreflen): #Berechnet BP
        BP=0
        if geshyplen>gesreflen:
            BP=1
        else:
            BP=math.exp(1-(gesreflen/geshyplen))
        return BP

    def pn(ref,hyp,n):
        for s in range(0,len(hyp)+1-n): #Verändert die Liste, so dass hyp[i] n Wörter ausgehend von der iten Position (getrennt durch Leerzeichen) enthält
            for indn in range(1,n):
                hyp[s]=hyp[s]+" "+hyp[s+indn]
                
        for s in range(0,len(ref)+1-n): #Verändert die Liste, so dass ref[i] n Wörter ausgehend von der iten Position (getrennt durch Leerzeichen) enthält
            for indn in range(1,n):
                ref[s]=ref[s]+" "+ref[s+indn]  
        if n>1:    #Schneidet übrige einzelne Wörter am Ende ab
            del hyp[-(n-1):]
            del ref[-(n-1):]          
        hypcop=list(set(hyp)) #hypcop ist wie hyp nur ohne Dopplungen der Satzteile
        sumo=0
        sumu=0
        for ind in range(0,len(hypcop)): #Iteration über jedes einzigartige n-gramm in der Hypothese
            sumo+=min(hyp.count(hypcop[ind]),ref.count(hypcop[ind])) #Count zählt vorkommenshäufigkeit
            sumu+=hyp.count(hypcop[ind])  
        sumoundsumu = [sumo,sumu] #Beide Werte in Liste zusammenfassen
        return sumoundsumu
