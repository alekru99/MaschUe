import mxnet as mx
import numpy as np
import pickle

groesse_vektor = 20

"""" Metrics with ignore label """

class Accuracy(mx.metric.EvalMetric):
    """
    Calculates accuracy. Taken from MXNet and adapted to work with batch-major labels
    (reshapes (batch_size, time) -> (batch_size * time).
    Also allows defining an ignore_label/pad symbol
    """

    def __init__(self,
                 name='accuracy',
                 output_names=None,
                 label_names=None,
                 ignore_label=None):
        super(Accuracy, self).__init__(name=name,
                                       output_names=output_names,
                                       label_names=label_names,
                                       ignore_label=ignore_label)
        self.ignore_label = ignore_label

    def update(self, labels, preds):
        mx.metric.check_label_shapes(labels, preds)

        for label, pred_label in zip(labels, preds):
            if pred_label.shape != label.shape:
                pred_label = mx.nd.argmax_channel(pred_label)
            pred_label = pred_label.asnumpy().astype('int32')
            label = mx.nd.reshape(label, shape=(pred_label.size,)).asnumpy().astype('int32')

            mx.metric.check_label_shapes(label, pred_label)
            if self.ignore_label is not None:
                correct = ((pred_label.flat == label.flat) * (label.flat != self.ignore_label)).sum()
                ignore = (label.flat == self.ignore_label).sum()
                n = pred_label.size - ignore
            else:
                correct = (pred_label.flat == label.flat).sum()
                n = pred_label.size

            self.sum_metric += correct
            self.num_inst += n

def batchsep(batches):
    first_loop = True
    for batch in batches:
        if first_loop:
            source_windows = batch[0]
            target_windows = batch[1]
            target_labels = batch[2]
            first_loop = False
        else:
            source_windows = mx.ndarray.concat(source_windows, batch[0], dim=0)
            target_windows = mx.ndarray.concat(target_windows, batch[1], dim=0)
            target_labels = mx.ndarray.concat(target_labels, batch[2], dim=0)
    return((source_windows,target_windows,target_labels))



def rnn_train(batchsize, source_dict_len, target_dict_len, train_data, train_label, train_length, val_data, val_label, val_length, encoder_dim, decoder_dim, tmax, cpu, isLoad, load_checkpoint_nrm, start_learning_rate, hlt, maxtrain, checkpoint_save_count, evaluate_val_count,  modelpath, optim,p,wpe):
    Activationfunktion = "tanh"
    p=0.1
    print(train_length)
    model_prefix = modelpath+'/defaulttrainedrnn2'
    ############# Prepare Data ################################################
    mx.random.seed(8888)
    groesse_vector_source = int(source_dict_len/wpe)
    groesse_vector_target = int(target_dict_len/wpe)
    train_data = np.split(train_data, 2, axis=1)
    train_data = {'input_enc' : train_data[0], 'input_dec' : train_data[1], 'input_length' : train_length}
    train_iter = mx.io.NDArrayIter(train_data, train_label, batchsize)
    val_data = np.split(val_data, 2, axis=1)
    val_data = {'input_enc' : val_data[0], 'input_dec' : val_data[1], 'input_length' : val_length}
    val_iter = mx.io.NDArrayIter(val_data, val_label, batchsize)

    ############# Create NN ###################################################
    embedding_source = mx.sym.Variable('input_enc')
    embedding_target = mx.sym.Variable('input_dec')
    source_length = mx.sym.Variable('input_length')
    #shape = (batch_size, tmax)

    embedding_source = mx.sym.Embedding(data = embedding_source, name="embedding_enc", input_dim = source_dict_len, output_dim = groesse_vector_source)
    embedding_target = mx.sym.Embedding(data = embedding_target, name="embedding_dec", input_dim = target_dict_len, output_dim = groesse_vector_target)
    #shape = (batch_size, tmax, groesse_vector)
    #SR erwartet

    lstm_encoder = mx.rnn.LSTMCell(num_hidden = encoder_dim, prefix="lstm-encoder_")
    outputs_encoder, states_encoder = lstm_encoder.unroll(length = tmax, inputs = embedding_source, layout = 'NTC', merge_outputs = True)
    #outputs_encoder hat shape = (batch_size, t_max, encoder_dim)
    
    #outputs_encoder = mx.sym.swapaxes(outputs_encoder, 0,1)
    #encoded_sentence = mx.sym.SequenceLast(outputs_encoder, source_length, use_sequence_length=True)
    #encoded_sentence = mx.sym.reshape(encoded_sentence, (batchsize,1,encoder_dim))
    #encoded_sentence_stretched = mx.sym.tile(encoded_sentence, reps=(1,tmax,1))

    #input_decoder = mx.sym.concat(embedding_target, encoded_sentence_stretched, dim = 2)
    #shape = (batch_size, tmax, groesse_vector + encoder_dim)

    lstm_decoder = mx.rnn.LSTMCell(num_hidden = decoder_dim, prefix="lstm-decoder_")
    outputs_decoder, states_decoder = lstm_decoder.unroll(length = tmax, inputs = embedding_target, layout = 'NTC', merge_outputs = True)
    #Element von shape = (batch_size, t_max, decoder_dim)

    w = 2*decoder_dim
    v = mx.sym.Variable('v', shape=(w,1),init=mx.init.Xavier(factor_type="in", magnitude=2.34), dtype='float32')
    #Irgendeine Feature dimension
    x = outputs_encoder
    s = outputs_decoder
    #Beide shape batch_size, t_max, feature_dim (feature = encoder/decoder)
    x_expanded = mx.sym.expand_dims(x, axis=1, name="expand_x")
    #shape batch_size, 1, t_max, encoder_dim
    s_expanded = mx.sym.expand_dims(s, axis=2, name="expand_s")
    #shape batch_size, t_max, 1, decoder_dim
    x_big = mx.sym.tile(x_expanded, (1,tmax,1,1))
    x_big = mx.sym.Dropout(x_big, p, name="drop_x")
    s_big = mx.sym.tile(s_expanded, (1,1,tmax,1))
    s_big = mx.sym.Dropout(s_big, p*0.5, name="drop_s")
    #Beide shape batch_size, t_max, t_max, feature_dim
    combined = mx.sym.concat(x_big, s_big, dim=3)
    #shape batch_size, t_max, t_max, encoder_dim+decoder_dim
    pre_alpha = mx.sym.FullyConnected(combined, num_hidden=w, flatten=False, name="Attention")
    pre_alpha = mx.sym.Activation(pre_alpha,"tanh")
    #shape batch_size, t_max, t_max, w
    alpha_dach = mx.sym.dot(pre_alpha, v)
    #shape batch_size, t_max, t_max, 1
    alpha_dach = mx.sym.reshape(alpha_dach, (batchsize, tmax, tmax))
    #shape batch_size, t_max, t_max
    alpha_dach = mx.sym.swapaxes(alpha_dach, 1, 2)
    alpha_dach = mx.sym.SequenceMask(alpha_dach, source_length, True, name="sequencemask", axis=1, value=-99999)
    alpha_dach = mx.sym.swapaxes(alpha_dach, 1, 2)

    alpha = mx.sym.softmax(alpha_dach, axis=2, name="alpha")
    #shape batch_size, t_max, t_max
    alpha_expanded = mx.sym.expand_dims(alpha, axis=3, name="expand_alpha")
    #shape batch_size, t_max, t_max, 1
    pre_c = mx.sym.broadcast_mul(x_big, alpha_expanded, name="pre_c")
    #shape batch_size, t_max, t_max, encoder_dim+decoder_dim
    c = mx.sym.sum(pre_c, axis=2, name="c")
    #shape batch_size, t_max, 1, encoder_dim+decoder_dim
    c = mx.sym.reshape(c, (batchsize, tmax, encoder_dim))
    #shape batch_size, t_max, encoder_dim+decoder_dim
    bypass = mx.sym.concat(c, s, dim=2)
    #bypass = mx.sym.concat(bypass, x, dim=2)
    #bypass = mx.sym.FullyConnected(bypass, num_hidden=3*w, flatten=False, name="pre_proj")
    




    '''
    #Attentionlayer von hier
    a_matrix = mx.sym.Variable('A', shape=(w,encoder_dim),init=mx.init.Xavier(factor_type="in", magnitude=2.34), dtype='float32')
    b_matrix = mx.sym.Variable('B', shape=(w,decoder_dim),init=mx.init.Xavier(factor_type="in", magnitude=2.34), dtype='float32')
    b = mx.sym.Variable('b', shape=(w,1,1),init=mx.init.Xavier(factor_type="in", magnitude=2.34), dtype='float32')
    v = mx.sym.Variable('v', shape=(w,1),init=mx.init.Xavier(factor_type="in", magnitude=2.34), dtype='float32')
    a_mat = mx.sym.Dropout(a_matrix, p, name='drop_a')
    x = outputs_encoder 
    s = outputs_decoder
    x_big = mx.sym.swapaxes(x, dim1=0, dim2=2, name='swapx')
    Ax = mx.sym.dot(lhs=a_mat,rhs=x_big)
    bigb = mx.sym.tile(b, reps=(1,tmax,batchsize))
    vt = mx.sym.transpose(v,name='vt')
    s_after = []
    for i in range(len(s)):
        s_after.append(mx.sym.swapaxes(s[i], dim1=0, dim2=1, name='swaps'+str(i)))
        s_after[i] = mx.sym.reshape(s_after[i], (decoder_dim, 1, batchsize))
        s_after[i] = mx.sym.Dropout(s_after[i], p*0.5, name='drop_s'+str(i))
        s_after[i] = mx.sym.tile(s_after[i], reps=(1,tmax,1))
        s_after[i] = mx.sym.dot(lhs=b_matrix,rhs=s_after[i])
        s_after[i] = (Ax+s_after[i]+bigb)
        s_after[i] = mx.sym.Activation(s_after[i], act_type=Activationfunktion, name='Acts'+str(i))
        s_after[i] = mx.sym.dot(lhs=vt,rhs=s_after[i])
        s_after[i] = mx.sym.softmax(s_after[i], axis=1, name='softmax'+str(i))
        s_after[i] = mx.sym.reshape(s_after[i], (tmax,batchsize))
        s_after[i] = mx.sym.swapaxes(s_after[i], dim1=0, dim2=1, name='alpha'+str(i))
        s_after[i] = mx.sym.reshape(s_after[i], (batchsize,tmax,1))
        s_after[i] = mx.sym.broadcast_mul(lhs=x, rhs=s_after[i])
        s_after[i] = mx.sym.sum(s_after[i], axis=1)
        s_after[i] = mx.sym.concat(s_after[i],s[i], dim=1)
        s_after[i] = mx.sym.reshape(s_after[i], (batchsize,1,encoder_dim+decoder_dim))
    for j in range(len(s_after)-1):
        s_after[j+1] = mx.sym.concat(s_after[j],s_after[j+1], dim=1)
    attention_out = s_after[-1]
    #Attentionlayer bis hier
    '''




    net = mx.sym.FullyConnected(bypass, name='projektionslayer', num_hidden = target_dict_len, flatten = False)
    net = mx.sym.SoftmaxOutput(net, name = 'softmax', use_ignore=True, ignore_label=0, preserve_shape = True)

    ############# Prepare NN ###################################################
    if (isLoad):
        net, arg_params, aux_params = mx.rnn.load_rnn_checkpoint([lstm_encoder, lstm_decoder],model_prefix, load_checkpoint_nrm)
    if(cpu):
        mod=mx.mod.Module(symbol=net, context=mx.cpu(), data_names=['input_enc','input_dec','input_length'], label_names=['softmax_label'])
    else:
        mod=mx.mod.Module(symbol=net, context=mx.gpu(), data_names=['input_enc','input_dec','input_length'], label_names=['softmax_label'])
    
    mod.bind(data_shapes=[('input_enc',(batchsize,tmax)), ('input_dec',(batchsize,tmax)),('input_length',(batchsize,))], label_shapes=train_iter.provide_label)
    if (isLoad):
        mod.set_params(arg_params=arg_params, aux_params=aux_params)
    else:
        mod.init_params(initializer=mx.init.Xavier(factor_type="in", magnitude=2.34))

    mod.init_optimizer(optimizer=optim, optimizer_params=(('learning_rate',start_learning_rate), ))
    print(start_learning_rate)
    metric_acc = Accuracy(ignore_label=0)
    metric_perp = mx.metric.Perplexity(ignore_label=0)
    epoch = 1
    if(isLoad):
        epoch = load_checkpoint_nrm +1
    new_perp = 0
    learnratetest=hlt

    while(epoch<=maxtrain):
        train_iter.reset()
        val_iter.reset()
        metric_acc.reset()
        metric_perp.reset()
        for batch in train_iter:
            lstm_encoder.reset()
            lstm_decoder.reset()
            mod.forward(batch, is_train=True)
            preds = [pNTC.reshape((batchsize*int(tmax),target_dict_len)) for pNTC in mod.get_outputs()]
            labels = [lNT.reshape((batchsize*int(tmax),)) for lNT in batch.label]
            metric_acc.update(labels, preds)
            metric_perp.update(labels, preds)
            mod.backward()
            mod.update()
        print('Epoch %d, Training %s %s' % (epoch, metric_acc.get(), metric_perp.get()))
        
        
        if (epoch % checkpoint_save_count == 0):
            mx.rnn.save_rnn_checkpoint([lstm_encoder, lstm_decoder],model_prefix, epoch, net, mod.get_params()[0], mod.get_params()[1])
            print("Saved Checkpoint for Epoch", epoch)
        val_iter.reset()
        if (epoch % evaluate_val_count == 0):
            '''
            Oder macht man jetzt:
                preds = [pNTC.reshape((batch_size*tmax,len(target_dict_len))) for pNTC in mod.get_outputs()]
                labels = [lNT.reshape((batch_size * tmax,)) for lNT in batch.label]
                metric_acc.update(labels, preds)

                Kann man so auch Perplexity updaten?
            '''
            metric_acc.reset()
            metric_perp.reset()
            for val_batch in val_iter:
                lstm_encoder.reset()
                lstm_decoder.reset()
                mod.forward(val_batch, is_train=False)
                preds = [pNTC.reshape((batchsize*int(tmax),target_dict_len)) for pNTC in mod.get_outputs()]
                labels = [lNT.reshape((batchsize*int(tmax),)) for lNT in val_batch.label]
                metric_acc.update(labels, preds)
                metric_perp.update(labels, preds)
            print('Epoch %d, Dev: %s %s' % (epoch, metric_acc.get(), metric_perp.get()))

            '''
            old_perp = new_perp
            new_perp = score[1][1]
            if (epoch>1) and is_reduce_learning_rate and (abs(1-(old_perp/new_perp)) < learnratetest):
                learning_rate = learning_rate/2.
                learnratetest = learnratetest/2.
                mod.init_optimizer(optimizer_params=(('learning_rate',learning_rate), ),force_init=True)
                print("Changed learnrate")
            ''' 
        epoch = epoch + 1






