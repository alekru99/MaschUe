import numpy as np
import mxnet as mx
from operator import itemgetter

BATCH_SIZE = 1

#source_file übergeben als Liste(Sätze) von Listen(Wörter als Indizes) 
def search_File(beamsize, checkpointind, networkpath, source_file, isCpu, target_dict, source_dict_len, target_dict_len, encoder_dim, decoder_dim, tmax):
    model_prefix = networkpath+"/defaulttrainedrnn"
    groesse_vector_source = int(source_dict_len/100)
    groesse_vector_target = int(target_dict_len/100)
    
    #Create encoder NN
    #Input Satz, der encoded werden soll mit shape (1, tmax)
    embedding_source = mx.sym.Variable('input_enc')
    #Länge des Satzes mit shape (1,)
    encoder_length = mx.sym.Variable('input_length')
    #danach shape = (1, tmax, groesse_source_vector)
    embedding_source = mx.sym.Embedding(data = embedding_source, input_dim = source_dict_len, output_dim = groesse_vector_source, name = 'embedding0')
    
    lstm_encoder = mx.rnn.LSTMCell(num_hidden = encoder_dim, prefix="lstm-encoder_")
    #danach shape = (1, tmax, encoder_dim)
    outputs_encoder, states_encoder = lstm_encoder.unroll(length = tmax, inputs = embedding_source, layout = 'NTC', merge_outputs = True)
    
    outputs_encoder = mx.sym.swapaxes(outputs_encoder, 0, 1)
    encoder = mx.sym.SequenceLast(outputs_encoder, encoder_length, use_sequence_length=True, name="out")
    #jetzt shape = (1, encoder_dim)

    #Create decoder NN
    #Das ist das Symbol, welches aus dem Encoder kommt mit shape = (1, encoder_dim)
    sentence = mx.sym.Variable('sentence')
    #Letztes Wort, das übersetzt wurde mit shape (1,)
    embedding_target = mx.sym.Variable('input_dec')
    
    #Danach shape = (1,groesse_target_vector)
    embedding_target = mx.sym.Embedding(data = embedding_target, input_dim = target_dict_len, output_dim = groesse_vector_target)

    ################ Testen ob die Dimension stimmt
    input_decoder = mx.sym.concat(embedding_target, sentence, dim = 1)
    #jetzt shape (1, groesse_target_vector + encoder_dim)
    lstm_decoder = mx.rnn.LSTMCell(num_hidden = decoder_dim, prefix="lstm-decoder_")
    begin_state = lstm_decoder.begin_state()
    outputs_decoder, states_decoder = lstm_decoder(input_decoder, begin_state)
    #shape = (1, decoder_dim)

    fully_connected = mx.sym.FullyConnected(outputs_decoder, name='projektionslayer', num_hidden = target_dict_len, flatten = False)
    #shape = (1, target_dict_len)

    #Die Labels sollen shape (batch_size, tmax) haben. Das funktioniert aber nicht. preserve_shape = True heißt, dass softmax entland axis=-1 berechnet wird
    decoder = mx.sym.SoftmaxOutput(fully_connected, name = 'softmax', use_ignore=True, ignore_label=0, preserve_shape = True)

    #Netzwerk des Trainings laden

    _, arg_params, aux_params = mx.rnn.load_rnn_checkpoint([lstm_encoder,lstm_decoder],model_prefix,checkpointind, )

    if(isCpu):
        mod_encoder=mx.mod.Module(symbol=encoder, context=mx.cpu(), data_names=['input_enc','input_length'], label_names=None)
        mod_decoder=mx.mod.Module(symbol=decoder, context=mx.cpu(), data_names=['input_dec','sentence'], label_names=['softmax_label'])
    else:
        mod_encoder=mx.mod.Module(symbol=encoder, context=mx.gpu(), data_names=['input_enc','input_length'], label_names=None)
        mod_decoder=mx.mod.Module(symbol=decoder, context=mx.gpu(), data_names=['input_dec','sentence'], label_names=['softmax_label'])
    s = mx.nd.zeros((BATCH_SIZE,))
    source_sentence =mx.nd.zeros((1,tmax))
    data_enc = {'input_enc': source_sentence, 'input_length' : s }
    last_translated_word_index = mx.nd.zeros((BATCH_SIZE,))
    encoder_outputi = mx.nd.zeros((1,encoder_dim))
    data_dec = {'sentence': encoder_outputi, 'input_dec':last_translated_word_index}
    target_label_enc = mx.nd.zeros((BATCH_SIZE,))
    target_label_dec = mx.nd.zeros((BATCH_SIZE,))
    iter_enc = mx.io.NDArrayIter(data_enc, target_label_enc, BATCH_SIZE)
    iter_dec = mx.io.NDArrayIter(data_dec, target_label_dec, BATCH_SIZE)
    mod_encoder.bind(data_shapes=iter_enc.provide_data)
    mod_decoder.bind(data_shapes=iter_dec.provide_data)

    mod_encoder.set_params(arg_params, aux_params, allow_extra=True)
    mod_decoder.set_params(arg_params, aux_params, allow_extra=True)

    translated_file = []
    for source_sentence in source_file:
        encoder.reset()
        decoder.reset()
        target_sentence = beam_Search_Line(beamsize, source_sentence, encoder, decoder, target_dict)
        translated_file.append(target_sentence)
    
    return translated_file



def greedy_Search_Line(beamsize, source_sentence, encoder, decoder, target_dict):
    source_sentence_length = len(source_sentence)
    s = mx.nd.zeros((BATCH_SIZE,))
    s[0] = source_sentence_length
    data_enc = {'input_enc': source_sentence, 'input_length' : s }
    target_label_enc = mx.nd.zeros((BATCH_SIZE,))
    iter_enc = mx.io.NDArrayIter(data_enc, target_label_enc, BATCH_SIZE)
    encoder.bind(data_shapes=iter_enc.provide_data, label_shapes=iter_enc.provide_label)
    for batch in iter_enc:
        encoder.forward(batch, is_train=False)
    
    #muss vielleicht Index null weglassen
    encoder_output = encoder.outputs[0] # shape (1, encoder_dim)
    target_sentence = [target_dict["<s>"]]

    counter = 0
    while(not target_sentence[-1] == target_dict["</s>"] and not counter == 3*source_sentence_length):
        last_translated_word_index = mx.nd.zeros((BATCH_SIZE,))
        last_translated_word_index[0] = target_sentence[-1]
        data_dec = {'sentence': encoder_output, 'input_dec':last_translated_word_index}
        target_label_dec = mx.nd.zeros((BATCH_SIZE,))
        iter_dec = mx.io.NDArrayIter(data_dec, target_label_dec, BATCH_SIZE)
        decoder.bind(data_shapes=iter_dec.provide_data, label_shapes=iter_dec.provide_label)
        for batch in iter_dec:
        	decoder.forward(batch, is_train=True)
        distribution = decoder.outputs[0]
        print(decoder.outputs.shape)
        translated_word_index = numpy.argmax(distribution)
        target_sentence.append(translated_word_index)
        counter = counter + 1

    return target_sentence

def beam_Search_Line(beamsize, source_sentence, encoder, decoder, target_dict):
    source_sentence_length = len(source_sentence)
    target_label_dec = mx.nd.zeros((BATCH_SIZE,))
    s = mx.nd.zeros((1,))
    s[0][0] = source_sentence_length
    data_enc = {'input_enc': source_sentence, 'input_length' : s }
    target_label_enc = mx.nd.zeros((BATCH_SIZE,))
    iter_enc = mx.io.NDArrayIter(data_enc, target_label_enc, BATCH_SIZE)
    encoder.bind(data_shapes=iter_enc.provide_data, label_shapes=iter_enc.provide_label)
    
    encoder.forward()
    #muss vielleicht Index null weglassen
    encoder_output = encoder.outputs[0].asnumpy()
    ltw = mx.nd.zeros((BATCHSIZE,))
    last_translated_word = target_dict["<s>"]
    ltw[0][0] = last_translated_word

    
    data_dec = {'sentence': encoder_output, 'input_dec':ltw}
    iter_dec = mx.io.NDArrayIter(data_dec, target_label_dec, BATCH_SIZE)
    decoder.bind(data_shapes=iter_dec.provide_data, label_shapes=iter_dec.provide_label)
    decoder.forward()
    distribution = decoder.outputs[0].asnumpy()
    distribution_sorted = numpy.argsort(distribution)
    distribution_sorted = distribution_sorted[::-1]

    tupel_prob_list = [([distribution_sorted[k]], distribution[distribution_sorted[k]]) for k in range(beamsize)]

    translated_words_number = 1
    while translated_words_number < 3*source_sentence_length:
        for i in range(beamsize):
            if(tupel_prob_list[i][0][-1]!=target_dict['</s>']):
                decoder.reset()
                for translated_word in range(translated_words_number):
                    last_translated_word = tupel_prob_list[i][0][translated_word]
                    decoder_input = mx.nd.zeros((1,))
                    decoder_input[0][0] = last_translated_word
                    data_dec = {'sentence': encoder_output, 'input_dec':decoder_input}
                    iter_dec = mx.io.NDArrayIter(data_dec, target_label_dec, BATCH_SIZE)
                    decoder.bind(data_shapes=iter_dec.provide_data, label_shapes=iter_dec.provide_label)
                    decoder.forward()
                distribution = decoder.outputs[0].asnumpy()
                distribution_sorted = numpy.argsort(distribution)
                distribution_sorted = distribution_sorted[::-1]
                for j in range(beamsize):
                    tempindexlist = list(tupel_prob_list[i][0])
                    tempindexlist.append(distribution_sorted[j])
                    
                    tupel_prob_list.append((list(tempindexlist), tupel_prob_list[i][1]*distribution[distribution_sorted[j]]))
                    tupel_prob_list[i][1] *= distribution[target_dict['</s>']]
                    tempindexlist.append(target_dict['</s>'])
                    tempindexlist = list(tupel_prob_list[i][0])
                tupel_prob_list[i][0] = list(tempindexlist)
        tupel_prob_list = sorted(tupel_prob_list,key=itemgetter(1))
        tupel_prob_list = tupel_prob_list[::-1]
        tupel_prob_list = tupel_prob_list[:beamsize]
        translated_words_number += 1

