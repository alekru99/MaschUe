import numpy
import mxnet as mx
import pickle
import math
from enum import Enum

windowSize = 2

class SearchMethod(Enum):
    Greedy = 0
    Beam = 1
    Batchbeam = 2

#Vokabular laden
dict_sc = open('defaultdict.de', "rb")
dict_tc = open('defaultdict.en', "rb")
dicttc = pickle.load(dict_tc)
dictsc = pickle.load(dict_sc)

def initBatchWithFirstWord(inputline):
    source_window = mx.nd.zeros((1, 2 * windowSize + 1))
    target_window = mx.nd.zeros((1, windowSize))
    target_label = mx.nd.zeros((1))

    #first word
    for ind in range(0, windowSize+1):
        #fill left target_window and left half of source_window with 1
        if not ind == windowSize:
            source_window[0][ind] = 1
            target_window[0][ind] = 1
        #fill rest of target_window with words from input line.
        if ind < len(inputline):
            source_window[0][ind + windowSize] = inputline[ind]
        #no word from input line is left, so we fill with 2
        else: 
            source_window[0][ind + windowSize] = 2

    return source_window, target_window, target_label

# unser netzwerk funktioniert nur mit windowSize = 2
def uebersetzen(method, beamsize, checkpointind, networkpath, inputline):
    if(method == 0):
        return greedy(checkpointind, networkpath, inputline)
    if(method == 1):
        return beam(beamsize, checkpointind, networkpath, inputline)
    if(method == 2):
        return batchbeam(beamsize,checkpointind, networkpath, inputline)


def greedy(checkpointind, networkpath, inputline):

    translation = []

    #Eingabe batch basteln
    source_window, target_window, target_label = initBatchWithFirstWord(inputline)

    #Netzwerk laden
    data = {'train_source_window':source_window, 'train_target_window': target_window}
    nd_iter = mx.io.NDArrayIter(data, target_label, 1)
    mod = mx.mod.Module.load(networkpath, checkpointind, False,\
        data_names=['train_source_window','train_target_window'],\
        label_names=['softmax_label'])
    mod.bind(data_shapes=nd_iter.provide_data, label_shapes=nd_iter.provide_label)
    
    #translate the first word greedily
    translation.append(greedyline(mod,nd_iter))

    #iterate over words
    current_word = 0
    #Suche nächstes Wort bis der Satz zu lang wird oder beendet ist
    while translation[current_word]!=2 and current_word<=len(inputline)*3:
        current_word = current_word+1
        #Schiebe alle Wörter im source_window nach links
        for ind in range(0,2*windowSize):
            source_window[0][ind] = source_window[0][ind+1].asnumpy()
        #Schiebe alle Wörter im target_window nach links
        for ind in range(0,windowSize-1):
            target_window[0][ind] = target_window[0][ind+1].asnumpy()
        #Wenn noch nicht am Satzende, fülle das letzte Wort des source_windows auf
        if(windowSize+current_word<len(inputline)):
            source_window[0][2*windowSize] = inputline[windowSize+current_word]
        #Sonst, fülle mit 2 auf
        else:
            source_window[0][2*windowSize] = 2
        #Das target_window wird mit dem letzten Wort aus der Übersetzung gefüllt
        target_window[0][windowSize-1] = translation[current_word-1]

        data = {'train_source_window':source_window, 'train_target_window': target_window}
        nd_iter = mx.io.NDArrayIter(data, target_label, 1)
        #Übersetze das nächste Wort
        translation.append(greedyline(mod,nd_iter))

    #output
    return(translation)

def greedyline (mod, nd_iter):
    for batch in nd_iter:
        mod.forward(batch, is_train=False)
    softout = mod.get_outputs()[0].asnumpy()
    return numpy.argmax(softout)


def beam (beamsize,checkpointind, networkpath, inputline):
    best_translations_with_probs = []
    new_word_candidates_with_distr = []
    windowsize = 2
    source_window, target_window, target_label = initBatchWithFirstWord(inputline)

    data = {'train_source_window':source_window, 'train_target_window': target_window}
    nd_iter = mx.io.NDArrayIter(data, target_label, 1)
    net, arg_params, aux_params = mx.rnn.load_rnn_checkpoint([lstm_encoder, lstm_decoder],model_prefix, load_checkpoint_nrm)
    mod=mx.mod.Module(symbol=net, context=mx.cpu(), data_names=['input_enc','input_dec','input_length'], label_names=['softmax_label'])
    mod.bind(data_shapes=nd_iter.provide_data, label_shapes=nd_iter.provide_label)
    mod.set_params(arg_params=arg_params, aux_params=aux_params)

    #Berechne die Indizes der besten Wörter und speichere mit der Wahrscheinlichkeitsverteilung
    new_word_candidates_with_distr.append(beamline(beamsize,mod,nd_iter))
    candidate_translations_with_probs = []
    for word_pos in range(0, beamsize):
        current_translation = []
        #Speichere für jeden der 5 Indizes und seine Wahrscheinlichkeit
        new_word_index = new_word_candidates_with_distr[0][0][0][word_pos]
        current_probability = new_word_candidates_with_distr[0][1][0][new_word_index]
        #Speichere Satz (hier nur ein Wort)
        current_translation.append((numpy.asscalar(new_word_index)))
        #Speichere Satz (hier nur ein Wort) mit seiner Wahrscheinlichkeit
        candidate_translations_with_probs.append((current_translation,current_probability))
    #Speichere die besten Sätze mit ihren Wahrscheinlichkeiten
    best_translations_with_probs.append(candidate_translations_with_probs)

    #iterate over words
    current_word_pos = 0
    #Sanity Check
    while current_word_pos<=len(inputline)*3:
        current_word_pos += 1
        #Verschiebe Elemente in den Windows nach links
        for ind in range(0,2*windowSize):
            source_window[0][ind] = source_window[0][ind+1].asnumpy()
        #Fülle source_window mit nächstem Wort vom Satz auf, falls möglich
        if(windowSize+current_word_pos<len(inputline))  :
            source_window[0][2*windowSize] = inputline[windowSize+current_word_pos]
        else:
            source_window[0][2*windowSize] = 2

        new_word_candidates_with_distr = []

        #Für jede der Top-5-Übersetzungen mit einem Wort weniger, passe target
        #windows an
        for pos in range(0,beamsize):
            for windowindex in range (0,windowsize):
                #Passe target_window an, je nach vorheriger Übersetzung
                if((current_word_pos-windowsize+windowindex)>=0):
                    target_window[0][windowindex] = best_translations_with_probs\
                        [current_word_pos-1][pos][0][current_word_pos-windowsize+windowindex]
                else:
                    target_window[0][windowindex] = 1
                    
            data = {'train_source_window':source_window,\
                'train_target_window': target_window}
            nd_iter = mx.io.NDArrayIter(data, target_label, 1)
            #Speichere die besten 5 Indizes mit der dazugehörenden Verteilung
            new_word_candidates_with_distr.append(beamline(beamsize,mod,nd_iter))
        
        candidate_translations_with_probs = []
        
        #Für jede der 5 besten Übersetzungen mit einem Wort weniger
        for potential_trans_path in range(0,len(new_word_candidates_with_distr)):
            #Finde heraus, wie das vorherige Wort übersetzt wurde
            previous_word_index = best_translations_with_probs\
                [current_word_pos-1][potential_trans_path][0][current_word_pos-1]
            #Für jede der 5 potentiellen besten Fortsetzung der Übersetzung
            for word_pos in range(0,beamsize):
                current_translation = []
                #Speichere den index des nächsten Worts
                current_word_index = new_word_candidates_with_distr\
                    [potential_trans_path][0][0][word_pos]
                #Noch nicht am Satzende    
                if(previous_word_index!=2):
                    #Was war die Wahrscheinlichkeit an dieser Stelle mit 
                    #dem momentanen Wort zu übersetzen
                    current_probability = new_word_candidates_with_distr\
                        [potential_trans_path][1][0][current_word_index]
                    #Normiere über die Wurzel
                    current_probability = (current_probability *\
                        (best_translations_with_probs[current_word_pos-1]\
                        [potential_trans_path][1])**(current_word_pos))**(1/(current_word_pos+1))
                    #Forme den momentanen Übersetzungssatz
                    current_translation = list(best_translations_with_probs[current_word_pos-1][potential_trans_path][0])
                    current_translation.append(current_word_index)
                    #Speichere den Satz mit seiner Wahrscheinlichkeit
                    candidate_translations_with_probs.append((current_translation,\
                        current_probability))
            # Schon am Satzende angekommen
            if(previous_word_index==2):
                #Füge einfach noch einmal Satzende zeichen an
                liste = list(best_translations_with_probs[current_word_pos-1]\
                    [potential_trans_path][0])
                liste.append(2)
                #Ändere die Wahrscheinlichkeit nicht und speichere ab
                candidate_translations_with_probs.append\
                    ((liste,best_translations_with_probs[current_word_pos-1]\
                    [potential_trans_path][1]))
        #Ordne die Kandidatenübersetzungen nach Wahrscheinlichkeiten
        candidate_translations_with_probs = sorted(candidate_translations_with_probs,\
            key=lambda x:x[1])
        #Behalte die 5 besten
        candidate_translations_with_probs = candidate_translations_with_probs[-beamsize:]
        best_translations_with_probs.append(candidate_translations_with_probs)

    #output
    for n in range(0,beamsize):
        best_translations_with_probs[len(best_translations_with_probs)-1][n][0].append(2)
        ind=best_translations_with_probs[len(best_translations_with_probs)-1][n][0].index(2)
        best_translations_with_probs[len(best_translations_with_probs)-1][n]=\
            (best_translations_with_probs[len(best_translations_with_probs)-1][n][0][:ind+1],\
            best_translations_with_probs[len(best_translations_with_probs)-1][n][1])
    output = []
    #print(best_translations_with_probs[len(best_translations_with_probs)-1])
    for n in range(0,beamsize):
        #for index in range(0,len(best_translations_with_probs[len(best_translations_with_probs)-1][n][0])):
            #print(dicttc[best_translations_with_probs[len(best_translations_with_probs)-1][n][0][index]])
        output.append(best_translations_with_probs[len(best_translations_with_probs)-1][n][0])
    #print(output)
    return output
    #for index in best_translations_with_probs:
        #print(dicttc[index])

def beamline (beamsize, mod, nd_iter):
    for batch in nd_iter:
        mod.forward(batch, is_train=False)
    softout = mod.get_outputs()[0].asnumpy()
    
    sortedsoftmax=numpy.argsort(softout)[0]
    return ([sortedsoftmax[(-beamsize):]],softout)

def batchbeam (beamsize,checkpointind, networkpath, inputline):
    best_translations_with_probs = []
    windowsize = 2
    source_window, target_window, target_label = initBatchWithFirstWord(inputline)

    data = {'train_source_window':source_window, 'train_target_window': target_window}
    nd_iter = mx.io.NDArrayIter(data, target_label, 1)
    mod = mx.mod.Module.load(networkpath, checkpointind, False, data_names=\
        ['train_source_window','train_target_window'], label_names=['softmax_label'])
    mod.bind(data_shapes=nd_iter.provide_data, label_shapes=nd_iter.provide_label)

    new_word_candidates_with_distr =beamline(beamsize,mod,nd_iter)
    candidate_translations_with_probs = []
    for word_pos in range(0,beamsize):
        current_translation = []
        new_word_index = new_word_candidates_with_distr[0][0][word_pos]
        current_probability = new_word_candidates_with_distr[1][0][new_word_index]
        current_translation.append((numpy.asscalar(new_word_index)))
        candidate_translations_with_probs.append((current_translation,current_probability))
    best_translations_with_probs.append(candidate_translations_with_probs)
    big_source_window = mx.nd.zeros((beamsize,2*windowSize+1))
    big_target_window = mx.nd.zeros((beamsize,windowSize))
    big_target_label = mx.nd.zeros((beamsize))
    for beamind in range(0,beamsize):
        for ind in range(0,2*windowSize+1):
            big_source_window[beamind][ind] = source_window[0][ind].asnumpy()
        for ind in range(0,windowsize):
            big_target_window[beamind][ind] = target_window[0][ind].asnumpy()
    data = {'train_source_window':big_source_window, 'train_target_window': big_target_window}
    nd_iter = mx.io.NDArrayIter(data, big_target_label, beamsize)
    mod.reshape(data_shapes=nd_iter.provide_data, label_shapes=nd_iter.provide_label)
    #iterate over words
    current_word_pos = 0
    while current_word_pos<=len(inputline)*3:
        current_word_pos = current_word_pos+1
        for beamind in range(0,beamsize):
            for ind in range(0,2*windowSize):
                big_source_window[beamind][ind] = big_source_window[beamind][ind+1].asnumpy()
            if(windowSize+current_word_pos<len(inputline))  :
                big_source_window[beamind][2*windowSize] = inputline[windowSize+current_word_pos]
            else:
                big_source_window[beamind][2*windowSize] = 2
        

        for pos in range(0,beamsize):
            for windowindex in range (0,windowsize):
                if((current_word_pos-windowsize+windowindex)>=0):
                    big_target_window[pos][windowindex] = best_translations_with_probs\
                        [current_word_pos-1][pos][0][current_word_pos-windowsize+windowindex]
                else:
                    big_target_window[pos][windowindex] = 1

        data = {'train_source_window':big_source_window, 'train_target_window': big_target_window}
        nd_iter = mx.io.NDArrayIter(data, big_target_label, beamsize)
        #print(nd_iter)
        new_word_candidates_with_distr=batchbeamline(beamsize,mod,nd_iter)
        candidate_translations_with_probs = []
        for potential_trans_path in range(0,beamsize):
            previous_word_index = best_translations_with_probs[current_word_pos-1][potential_trans_path][0][current_word_pos-1]
            for word_pos in range(0,beamsize):
                current_translation = []
                current_word_index = new_word_candidates_with_distr[0][potential_trans_path][word_pos]
                if(previous_word_index!=2):
                    current_probability = new_word_candidates_with_distr[1][potential_trans_path][int(current_word_index)]
                    current_probability = (current_probability * (best_translations_with_probs[current_word_pos-1][potential_trans_path][1])**(current_word_pos))**(1/(current_word_pos+1))
                    current_translation = list(best_translations_with_probs[current_word_pos-1][potential_trans_path][0])
                    current_translation.append(current_word_index)
                    candidate_translations_with_probs.append((current_translation,current_probability))
            if(previous_word_index==2):
                liste = list(best_translations_with_probs[current_word_pos-1][potential_trans_path][0])
                liste.append(2)
                candidate_translations_with_probs.append((liste,best_translations_with_probs[current_word_pos-1][potential_trans_path][1]))
        candidate_translations_with_probs = sorted(candidate_translations_with_probs, key=lambda x:x[1])
        candidate_translations_with_probs = candidate_translations_with_probs[-beamsize:]
        best_translations_with_probs.append(candidate_translations_with_probs)

    #output
    for n in range(0,beamsize):
        best_translations_with_probs[len(best_translations_with_probs)-1][n][0].append(2)
        ind=best_translations_with_probs[len(best_translations_with_probs)-1][n][0].index(2)
        best_translations_with_probs[len(best_translations_with_probs)-1][n]=\
            (best_translations_with_probs[len(best_translations_with_probs)-1][n][0][:ind+1],\
                best_translations_with_probs[len(best_translations_with_probs)-1][n][1])
    output = []
    #print(best_translations_with_probs[len(best_translations_with_probs)-1])
    for n in range(0,beamsize):
        #for index in range(0,len(best_translations_with_probs[len(best_translations_with_probs)-1][n][0])):
            #print(dicttc[best_translations_with_probs[len(best_translations_with_probs)-1][n][0][index]])
        output.append(best_translations_with_probs[len(best_translations_with_probs)-1][n][0])
    #print(output)
    return output
    
def batchbeamline (beamsize,mod,nd_iter):
    for batch in nd_iter:
        mod.forward(batch, is_train=False)
    softout = mod.get_outputs()[0]
    #print((mx.ndarray.topk(softout, k=beamsize)).asnumpy())
    return ((mx.ndarray.topk(softout, k=beamsize)).asnumpy(),softout)
    #sortedsoftmax=numpy.argsort(softout)[0]
    #return ([sortedsoftmax[(-beamsize):]],softout)

#inputstrlist="der himmel ist blau .".split(" ")
#inputlist = [0]
#for word in inputstrlist:
#    inputlist.append(dictsc[word])
#inputlist.append(2)
#print(inputstrlist,inputlist)
#uebersetzen (2,3,10, "defaulmodel/defaulttrainednn", inputlist)
