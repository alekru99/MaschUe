import pickle
import neuralnetwork



dic_s = open("defaulmodel/defaultdict.de", "rb")
dic_t = open("defaulmodel/defaultdict.en", "rb")  
dicts = pickle.load(dic_s)
dictt = pickle.load(dic_t)
(batch, label, length) = pickle.load(open("defaulmodel/defaultcheckbatch", "rb"))
tmax = int(batch.shape[1]/2)
current_sentence = []
for i in range(batch.shape[0]):
    print("\nSource Sentence:")
    for j in range(tmax):
        if (batch[i][j]):
            current_sentence.append(dicts[int(batch[i][j].asnumpy().item(0))])
    print(" ".join(current_sentence))
    current_sentence = []
    print("\nTarget Sentence:")
    for j in range(tmax):
        if (batch[i][tmax+j]):
            current_sentence.append(dictt[int(batch[i][tmax+j].asnumpy().item(0))])
    print(" ".join(current_sentence))
    current_sentence = []
    print("\nTarget Label:")
    for j in range(tmax):
        if (label[i][j]):
            current_sentence.append(dictt[int(label[i][j].asnumpy().item(0))])
    print(" ".join(current_sentence))
    current_sentence = []
