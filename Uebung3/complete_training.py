import argparse
import batch
import pickle
import gzip
import bpe
import dictonary
import neuralnetwork

def f_alignment(index, words_e_len, words_f_len):
    return int((float(index) / words_e_len) * words_f_len)
#Source language files
parser = argparse.ArgumentParser()
parser.add_argument("-tds", "--strainingsdaten", help="zipped File of Sentences to train on Source language", dest="strain_path")
parser.set_defaults(strain_path="multi30k.de.gz")
parser.add_argument("-bfs", "--sbpesavefile", help="Saves BPE of trainingsdata and uses this for bpe apply Source language", dest="sbpe_path")
parser.set_defaults(sbpe_path="a1trained7k.deen")
parser.add_argument("-bas", "--sbpeapplicationfile", help="Saves Result of BPE application on trainingsdaten and is used for creation of dictionary Source language", dest="sbpe_appl_path")
parser.set_defaults(sbpe_appl_path="defaultbpeapply.de")
parser.add_argument("-dfs", "--sdictionaryfile", help="Saves Result of Dict creation and is used for creation of Batches Source language", dest="sdict_path")
parser.set_defaults(sdict_path="defaultdict.de")
#Entwicklungsdaten
parser.add_argument("-eds", "--sentwicklungsdaten", help="File of Sentences to test neural network on Source language", dest="scheck_path")
parser.set_defaults(scheck_path="multi30k.dev.de")
parser.add_argument("-basc", "--scheckbpeapplicationfile", help="Saves Result of BPE application on checkdaten and is used for creation of dictionary Source language", dest="scbpe_appl_path")
parser.set_defaults(scbpe_appl_path="defaultcheckbpeapply.de")
#Target language files
parser.add_argument("-tdt", "--ttrainingsdaten", help="zipped File of Sentences to train on Target language", dest="ttrain_path")
parser.set_defaults(ttrain_path="multi30k.en.gz")
parser.set_defaults(tbpe_path="defaultbpetraining.en")
parser.add_argument("-bat", "--tbpeapplicationfile", help="Saves Result of BPE application on trainingsdaten and is used for creation of dictionary Target language", dest="tbpe_appl_path")
parser.set_defaults(tbpe_appl_path="defaultbpeapply.en")
parser.add_argument("-dft", "--tdictionaryfile", help="Saves Result of Dict creation and is used for creation of Batches Target language", dest="tdict_path")
parser.set_defaults(tdict_path="defaultdict.en")
#Entwicklungsdaten
parser.add_argument("-edt", "--tentwicklungsdaten", help="File of Sentences to test neural network on Target language", dest="tcheck_path")
parser.set_defaults(tcheck_path="multi30k.dev.en")
parser.add_argument("-batc", "--tcheckbpeapplicationfile", help="Saves Result of BPE application on checkdaten and is used for creation of dictionary Target language", dest="tcbpe_appl_path")
parser.set_defaults(tcbpe_appl_path="defaultcheckbpeapply.en")
#Batch file
parser.add_argument("-bf", "--batchfile", help="Saves Result of Batches and is used for neural Network", dest="batch_path")
parser.set_defaults(batch_path="defaultbatch")
#Entwicklungsdaten
parser.add_argument("-bfc", "--cbatchfile", help="Saves Result of Checkdata Batches and is used for neural Network", dest="cbatch_path")
parser.set_defaults(cbatch_path="defaultcheckbatch")
#Parameters
parser.add_argument("-sz", "--subwortzerlegungen", help="Anzahl der Subwort-Zerlegung Operationen", type=int)
parser.set_defaults(subwortzerlegungen=15000)
parser.add_argument("-bs", "--batchsize", help="Batchgrösse", type=int)
parser.set_defaults(batchsize=200)
parser.add_argument("-ws", "--windowsize", help="Windowgrösse", type=int)
parser.set_defaults(windowsize=2)
parser.add_argument("-lc", "--layercount", help="Layer Anzahl", type=int)
parser.set_defaults(layercount=3)
parser.add_argument("-nc", "--neurocount", help="Neuronen pro Layer", type=int)
parser.set_defaults(neurocount=750)
parser.add_argument("-cd", "--checkdist", help="Anzahl der Durchläufe bis zum speichern eines Checkpoints", type=int)
parser.set_defaults(checkdist=2)
parser.add_argument("-ed", "--evaldist", help="Anzahl der Durchläufe bis zum testen auf den dev Daten", type=int)
parser.set_defaults(evaldist=1)
parser.add_argument("-ci", "--checkind", help="Index des zu ladenden Checkpoints", type=int)
parser.set_defaults(checkind=7)
parser.add_argument("-lr", "--learnrate", help="Lern Rate", type=float)
parser.set_defaults(learnrate=0.1)
parser.add_argument("-af", "--activationfunction", help="Aktivierungsfunktion", type=str)
parser.set_defaults(activationfunction="tanh")
parser.add_argument("-op", "--optimizer", help="Optimizer", type=str)
parser.set_defaults(optimizer="adadelta")
parser.add_argument("-hlt", "--halflearnthreashold", help="Schwellenwert für halbierung der Lernrate", type=float)
parser.set_defaults(halflearnthreashold=0.04)
#Execution selection
parser.add_argument("-nb", "--nobpe", help="Hier soll angegeben werden ob BPE nicht benötigt ist dann soll aber eine bpe Datei angegeben werden", dest="bpee",action="store_false")
parser.set_defaults(bpee=True)
parser.add_argument("-nbap", "--nobpeapply", help="Hier soll angegeben werden ob BPE apply nicht benötigt ist dann soll aber eine bpe apply Datei angegeben werden", dest="bpeapply",action="store_false")
parser.set_defaults(bpeapply=True)
parser.add_argument("-nd", "--nodict", help="Hier soll angegeben werden ob dict erstellung nicht benötigt ist dann soll aber eine dict Datei angegeben werden", dest="dic",action="store_false")
parser.set_defaults(dic=True)
parser.add_argument("-nba", "--nobatch", help="Hier soll angegeben werden ob batch erstellung nicht benötigt ist dann soll aber eine batch Datei angegeben werden", dest="batch",action="store_false")
parser.set_defaults(batch=True)
parser.add_argument("-nt", "--notraining", help="Hier soll angegeben werden ob Training Daten nicht verarbeitet werden sollen", dest="train",action="store_false")
parser.set_defaults(train=True)
parser.add_argument("-nch", "--nocheck", help="Hier soll angegeben werden ob Check Daten nicht verarbeitet werden sollen", dest="check",action="store_false")
parser.set_defaults(check=True)
parser.add_argument("-nn", "--noneuro", help="Hier soll angegeben werden ob NEur. Netz nicht trainiert werden sollen", dest="neuro",action="store_false")
parser.set_defaults(neuro=True)
parser.add_argument("-gpu", "--gpu", help="Hier soll angegeben werden ob auf gpu trainiert werden soll", dest="cpu",action="store_false")
parser.set_defaults(cpu=True)
parser.add_argument("-load", "--load", help="Hier soll angegeben werden ob checkpoint geladen werden soll", dest="load",action="store_true")
parser.set_defaults(load=False)
parser.add_argument("-hl", "--halflearning", help="Hier soll angegeben werden ob checkpoint geladen werden soll", dest="halflearning",action="store_true")
parser.set_defaults(load=False)
args = parser.parse_args()

traindatas = gzip.open(args.strain_path, "rt")
traindatat = gzip.open(args.ttrain_path, "rt")
checkdatas = open(args.scheck_path, "rt")
checkdatat = open(args.tcheck_path, "rt")

if(args.train):
    print("\nTrainingdata:")
    texts = []
    textt = []
    for line in traindatas:
        texts.append(line)
    for line in traindatat:
        textt.append(line)
    text = texts + textt
    

    if(args.bpee):
        bpe.train(text, args.subwortzerlegungen, args.sbpe_path)
  
    bpedats = open(args.sbpe_path, "rb")
    bpedatt = open(args.sbpe_path, "rb")  
    bpedatas = pickle.load(bpedats)
    bpedatat = pickle.load(bpedatt)

    if(args.bpeapply):
        print("\nBPE Source Application:")
        applieds = bpe.apply(texts,False, args.sbpe_path)
        with open(args.sbpe_appl_path, "wt") as bpeapplieds:
            print(applieds, file=bpeapplieds)
        print("\nBPE Target Application:")
        appliedt = bpe.apply(textt,False, args.sbpe_path)
        with open(args.tbpe_appl_path, "wt") as bpeappliedt:
            print(appliedt, file=bpeappliedt)

    bpeapps = open(args.sbpe_appl_path, "rt")
    bpeappt = open(args.tbpe_appl_path, "rt")

    if(args.dic):
        dictonary.Wraper.train(bpeapps, args.sdict_path)
        print("\nDone making Source Dict")
        dictonary.Wraper.train(bpeappt, args.tdict_path)
        print("\nDone making Target Dict")

    print("\nCreating Batch:")
    bpeapps = open(args.sbpe_appl_path, "rt")
    bpeappx = open(args.sbpe_appl_path, "rt")
    bpeappt = open(args.tbpe_appl_path, "rt")  
    dic_s = open(args.sdict_path, "rb")
    dic_t = open(args.tdict_path, "rb")  
    dicts = pickle.load(dic_s)
    dictt = pickle.load(dic_t)

    if(args.batch):
        all_batches = batch.createBatches(args.batchsize, args.windowsize, f_alignment, bpeappt, bpeapps, bpeappx, dictt, dicts)
        print("\nDone making Batch")
        pickle.dump(all_batches, open(args.batch_path, "wb"))

if(args.check):
    print("\nCheckdata:")
    textsc = []
    texttc = []
    for line in checkdatas:
        textsc.append(line)
    for line in checkdatat:
        texttc.append(line)
    

    bpedatsc = open(args.sbpe_path, "rb")
    bpedattc = open(args.sbpe_path, "rb")  
    bpedatasc = pickle.load(bpedatsc)
    bpedatatc = pickle.load(bpedattc)

    if(args.bpeapply):
        print("\nBPE Source Application:")
        appliedsc = bpe.apply(textsc,False, args.sbpe_path)
        with open(args.scbpe_appl_path, "wt") as bpeappliedsc:
            print(appliedsc, file=bpeappliedsc)
        print("\nBPE Target Application:")
        appliedtc = bpe.apply(texttc,False, args.sbpe_path)
        with open(args.tcbpe_appl_path, "wt") as bpeappliedtc:
            print(appliedtc, file=bpeappliedtc)

    

    print("Creating Batch:")
    bpeappsc = open(args.scbpe_appl_path, "rt")
    bpeappxc = open(args.scbpe_appl_path, "rt")
    bpeapptc = open(args.tcbpe_appl_path, "rt")  
    dic_sc = open(args.sdict_path, "rb")
    dic_tc = open(args.tdict_path, "rb")  
    dictsc = pickle.load(dic_sc)
    dicttc = pickle.load(dic_tc)

    if(args.batch):
        all_batches = batch.createBatches(args.batchsize, args.windowsize, f_alignment, bpeapptc, bpeappsc, bpeappxc, dicttc, dictsc)
        print("\nDone making Batch")
        pickle.dump(all_batches, open(args.cbatch_path, "wb"))

print("\nFinished preparing for neural network")
if(args.neuro):
    all_batches = pickle.load(open(args.batch_path, "rb"))
    all_cbatches = pickle.load(open(args.cbatch_path, "rb"))
    dic_sc = open(args.sdict_path, "rb")
    dic_tc = open(args.tdict_path, "rb")  
    dictsc = pickle.load(dic_sc)
    dicttc = pickle.load(dic_tc)
    neuralnetwork.neurotrain(all_batches, all_cbatches ,args.batchsize,args.layercount,args.neurocount,len(dictsc),len(dicttc),args.cpu,args.checkdist,args.evaldist, args.load, args.checkind, args.halflearning, args.learnrate, args.activationfunction, args.optimizer, args.halflearnthreashold)

