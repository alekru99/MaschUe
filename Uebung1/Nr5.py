#Eingabe der Form python3 N5.py Refkorpus Hypothesenkorpus1 .....

import argparse
import math
import Bleuclass
import Levenshteinclass

class WERcalc:
    def WER(ref, hyp):
        gesreflen=0
        geslevenshtein=0
        for i in range(0,len(ref)): #Summiert Levenshteindistanz und Referenzlaenge ueber alle Zeilen
                refe = ref[i].split()
                hypo = hyp[i].split()
                geslevenshtein+=Levenshteinclass.Levenshtein(refe,hypo,False)
                gesreflen+=len(refe)
        return geslevenshtein/gesreflen #Berechnen und Rückgabe des WER Quotienten
class PERcalc:
    def Ueb(refe,hypo): #Berechnet im Endeffekt Schnitt von zwei Mengen die Dopplungen erlauben (ref: "the cat the" und hyp: "the" ergeben eine Übereinstimmung
        U=0
        refer=list(refe)
        hypot=list(hypo)
        hyposet=list(set(hypot)) #hypothese ohne Dopplungen
        for el in hyposet: #Iteriert über alle Wörter und Summiert Anzahl der Übereinstimmungen auf
            U+=min(hypot.count(el),refer.count(el)) #Bsp.: "the cat is on the mat" und "the the the the the the the" haben zwei Übereinstimmungen für "the"
        return U

    def PER(ref, hyp):
        gesreflen=0
        geshyplen=0
        gesue=0
        for i in range(0,len(ref)): #Summiert Anzahl Uebereinstimmungen und Korpuslängen auf
                refe = ref[i].split()
                hypo = hyp[i].split()
                gesue+=PERcalc.Ueb(refe,hypo)
                gesreflen+=len(refe)
                geshyplen+=len(hypo)
        return (1-(gesue-max(0,geshyplen-gesreflen))/gesreflen) #Berechnet und returnt PER


#Anfang des Programms
print("\n Werte der Hypothesen: \n")
ref = []
hyp = []
hypothesen = []
Numberofsentence = 0

parser = argparse.ArgumentParser()
parser.add_argument('-ref',"--reference", help="Hier soll ein Refernzkorpus wie newtest.en angegeben werden", type=argparse.FileType('r'), required=True, nargs=1)
parser.add_argument('-hyp',"--hypothesis", help="Hier sollen Hypothesenkorpusse wie newtest.hyp1 angegeben werden", type=argparse.FileType('r'), required=True, nargs='+')
parser.add_argument('-N', "--N", help="Hier kann ein N ausgewählt werden", type=int, required=False, nargs=1)
N=4
args = parser.parse_args()
for line in args.reference[0]:
    ref.append(line)
for f in args.hypothesis:
    for line in f:
        hyp.append(line)
    hypothesen.append(hyp)
    hyp = []
if(args.N is not None):
    N=args.N[0]
if(N<1):
    N=4
    print("N zu klein default 4 verwendet")
Rangfolgerechnung = [0]*len(hypothesen) #Rangfolgerechnung[x] = Bleu + 1-WER + 1-PER (höher ist besser)
for i in range(1,len(hypothesen)+1): #Ausgabe der Werte für alle Hypothesen
    print ("Hypothese "+str(i)+":")
    Wer=WERcalc.WER(ref,hypothesen[i-1])
    Rangfolgerechnung[i-1]=2-Wer
    print("WER:",Wer)
    Per=PERcalc.PER(ref,hypothesen[i-1])
    Rangfolgerechnung[i-1]-=Per
    print("PER:",Per)
    Bleu=Bleuclass.BLEUcalc.BLEU(ref,hypothesen[i-1],N)
    Rangfolgerechnung[i-1]+=Bleu
    print("BLEU:",Bleu)
Rangfolge = [Rangfolgerechnung.index(x) for x in sorted(Rangfolgerechnung)] # Gibt an Rangfolge die Reihenfolge der sortierten Rangfolgerechnung Werte zurück
Rangfolge = Rangfolge[::-1]
print("\n Rangfolge:")
for i in range(0,len(Rangfolge)):
    print("Platz", str(i+1)+":","Hypothese",(Rangfolge[i]+1)) 
