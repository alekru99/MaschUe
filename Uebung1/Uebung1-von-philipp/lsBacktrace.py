import levenshtein
import argparse
import pickle

# Kommandozeilen Parameter deligieren
parser = argparse.ArgumentParser()
parser.add_argument("-ref", "--reference", required=True, help="Reference translation")
parser.add_argument("-hyp", "--hypothesis", required= True, help="Hypothesis translation")
parser.add_argument("-lineNum", "--lineNumber", type=int, required=True, help="Line Number for backtrace of LS")
args = parser.parse_args()

lineNum = args.lineNumber

if lineNum < 0 :
    print("You have to use a positive Integer for lineNum. For wrong input lineNum will have standard value 0.")
    lineNum = 0

# Uebersetzungen oeffnen und einlesen
ref = open(args.reference, 'r', encoding="utf-8")
hyp = open(args.hypothesis, 'r', encoding="utf-8")

# Referenz und Hypothese zeilenweise in einer Liste speichern
l = list(zip(ref, hyp))

# Liste in save datei speichern
pickle.dump(l, open("save.pkl", "wb"))

# geoeffnete Uebersetzungen schliessen
ref.close()
hyp.close()

L = pickle.load(open("save.pkl", "rb"))
ref = (''.join(L[lineNum])).splitlines()[0]
hyp = (''.join(L[lineNum])).splitlines()[1]
print("Levenshtein-Distance for Line {0} is {1}".format(lineNum, levenshtein.Levenshtein.levenshtein_distance(ref, hyp, True)))
