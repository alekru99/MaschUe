class Levenshtein:

    # berechnet Levenstein Distanz fuer zwei Strings und gibt Operationen aus falls showOp = true ist
    def levenshtein_distance(reff: str, hypp: str, showOp = False):
        # Matrix anlegen mit Laenge J+1 und Breite K+1
        J = len(reff.split())
        K = len(hypp.split())
        matrix = [[0 for x in range(K+1)] for y in range(J+1)]

        # Erste Zeile aufsteigend initialisieren
        for k in range(K+1):
            matrix[0][k] = k

        # Erste Spalte aufsteigend initialisieren
        for j in range(J+1):
            matrix[j][0] = j

        # Berechne Levenshtein Matrix
        for j in range(J):
            # Durchlaufe fuer jede Zeile jede Spalte
            for k in range(K):
                # wenn sich die Woerter unterscheiden trage das Minimum von allen Vorgaengern + 1 ein
                if reff.split()[j] != hypp.split()[k]:
                    matrix[j+1][k+1] = min(matrix[j][k], matrix[j+1][k], matrix[j][k+1]) + 1
                # sonst trage das Minimum vom horizontalen und vertikalen Vorgaenger + 1 und diagonalen Vorgaenger ein
                else:
                    matrix[j+1][k+1] = min(matrix[j][k], matrix[j+1][k] + 1, matrix[j][k+1] + 1)

        # Levenshtein Distanz steht im Knoten (J,K)
        levDis = matrix[J][K]

        # backtrace
        if showOp == True:
            # Initialisierung
            j = J
            k = K
            s = ""
            # gehe Matrix vom Knoten (J,K) durch bis Knoten (0,0) erreicht wird
            while j != 0 or k != 0:
                # wenn Kosten von oben + 1 gleich Kosten ist, dann wars eine Insertion
                if j>0 and (matrix[j-1][k] + 1 == matrix[j][k]):
                    s = "insert \'{0}\'\n{1}".format(reff.split()[j-1], s)
                    j=j-1
                # wenn Kosten von links + 1 gleich Kosten ist, dann wars eine Deletion
                elif k>0 and (matrix[j][k-1] + 1 == matrix[j][k]):
                    s = "delete \'{0}\'\n{1}".format(hypp.split()[k-1], s)
                    k=k-1
                # wenn Kosten von diagonal links oben + 1 gleich Kosten, dann wars eine Substitution
                elif j>0 and k>0 and (matrix[j-1][k-1] + 1 == matrix[j][k]):
                    s = "substitute \'{0}\' with \'{1}\'\n{2}".format(hypp.split()[k-1], reff.split()[j-1], s)
                    j=j-1
                    k=k-1
                # wenn Kosten von diagonal links oben gleich Kosten, dann wars ein Match
                elif j>0 and k>0 and (matrix[j-1][k-1] == matrix[j][k]):
                    s = "match \'{0}\'\n{1}".format(hypp.split()[k-1], s)
                    j=j-1
                    k=k-1
                # wenn nichts passt, dann ist irgendwas mit der Erstellung der Levenshtein Matrix schief gelaufen
                else:
                    print("Matrix Error")
                    break

            print(s)

        return levDis

"""
test = pickle.load(open("save.pkl", "rb"))
lineNum = 0
ref = (''.join(test[lineNum])).splitlines()[0]
hyp = (''.join(test[lineNum])).splitlines()[1]
print(levenshtein_distance(ref, hyp, True))
"""