import levenshtein
import pickle

class WERcalc:

    # berechnet word error rate
    def wer():
        # oeffne Daten
        L = pickle.load(open("save.pkl", "rb"))
        refCorpLen = 0
        accuLev = 0
        # akkumuliere Levenshtein Distanz und zaehle Referenzlaenge
        for l in L:
            ref = (''.join(l)).splitlines()[0]
            hyp = (''.join(l)).splitlines()[1]
            refCorpLen += len(ref.split())
            accuLev += levenshtein.Levenshtein.levenshtein_distance(ref, hyp)

        # WER = Levenshtein-distanz/Referenzlaenge
        return accuLev/refCorpLen

class PERcalc:

    def matches(h: str, r: str):
        hList = h.split()
        rList = r.split()
        count = 0
        # vergleiche jedes Wort aus der Hypothese mit jedem wort aus der Referenz
        for hword in hList:
            for i in range(len(rList)):
                # wenn die Woerter uebereinstimmen zaehle hoch, markiere das Referenz wort und weiter mit naechstem Wort
                if hword == rList[i]:
                    rList[i] = ""
                    count += 1
                    break

        return count

    def per():
        # oeffne Daten
        L = pickle.load(open("save.pkl", "rb"))
        refCorpLen = 0
        hypCorpLen = 0
        accuMatches = 0
        # akkumuliere Uebereinstimmungen und zaehle Referenzlaenge und Hypothesenlaenge
        for l in L:
            ref = (''.join(l)).splitlines()[0]
            hyp = (''.join(l)).splitlines()[1]
            refCorpLen += len(ref.split())
            hypCorpLen += len(hyp.split())
            accuMatches += PERcalc.matches(hyp, ref)

        # PER = 1 - (Uebereinstimmungen - max(0, Hyp.laenge - Ref.laenge))/Ref.laenge
        res = 1-((accuMatches - max(0, hypCorpLen - refCorpLen))/ refCorpLen)

        return res

