import pickle
import math

class BLEUcalc:

    # Hilffunktion: erzeugt fuer ein gegebenes n und einen gegebenen string eine List von n-grams
    def ngramlist(n: int, sentence: str):
        # erzeuge Liste aus den Woertern im Satz
        wordList = sentence.split()
        ngramList = []
        while len(wordList) >= n:
            ngram = ""
            # erzeuge ein ngram in dem die ersten n elemente von der Woerter Liste zu einem String werden
            for i in range(n):
                ngram = "{0} {1}".format(ngram, "".join(wordList[i]))

            # fuege erzeugten ngram in die ngram Liste hinzu und loesche erstes Element in der Woerter Liste
            ngramList.append(ngram)
            del wordList[0]

        return ngramList


    """
    berechne fuer gegebenes n  alle Uebereinstimmungen von ngrams in der Hypothese mit denen in der Referenz
    zusaetzlich berechne immer sum(min{#n-gram in r, #n-gram in h}) fuer n-gram element h wenn minActive True ist
    damit spaeter die Praezision einfacher zu berechnen ist
    """
    def matches(n: int, h: str, r: str, minActive: bool):
        hWordList = h.split()
        hngramList = BLEUcalc.ngramlist(n, h)
        rngramList = BLEUcalc.ngramlist(n, r)
        matchNum = 0
        matchNum_in_H = 0
        matchNum_in_R = 0

        while len(hWordList) >= n:

            # benutze aehnliches Verfahren wie bei ngramlist() um ein n-gram der Hypothese zu erstellen
            hgram = ""
            for i in range(n):
                hgram = "{0} {1}".format(hgram, "".join(hWordList[i]))

            # vergleiche nun n-gram der hypothese mit jedem element aus der rngramList
            for i in range(len(rngramList)):
                # wenn sie uebereinstimmen zaehle hoch und markiere das Element als benuntzt
                if hgram == rngramList[i]:
                    matchNum_in_R += 1
                    rngramList[i] = ""


            if minActive == True:
                # vergleiche zusaetlich mit das n-gram mit jedem element aus der hngramList
                for i in range(len(hngramList)):
                    if hgram == hngramList[i]:
                        matchNum_in_H += 1
                        hngramList[i] = ""

                # zaehle das minimum von beiden
                matchNum += min(matchNum_in_H, matchNum_in_R)
                matchNum_in_H = 0
                matchNum_in_R = 0

            # Loesche nun erstes Wort und wiederhole die While-Schleife. So wird das naechste n-gram erzeugt.
            del hWordList[0]

        if minActive == True:
            return matchNum
        else:
            return matchNum_in_R


    # berechnet die Brevity Penalty fuer Laenge der Referenz r und Laenge der Hypothese c
    def brevity_penalty(c: int, r: int):
        if c > r:
            return 1
        else:
            # bestaft zu kurze Hypothesen
            return math.exp(1 - (r / c))


    def bleu(N: int):
        L = pickle.load(open("save.pkl", "rb"))
        refCorpLen = 0
        hypCorpLen = 0
        Pn = []

        for n in range(1, N + 1):
            Pn.append(0)

        # rechne fuer jedes Satzpaar die Satzlaenge fuer BP und die Praezision aus
        for n in range(1, N + 1):
            sum1 = 0
            sum2 = 0
            for l in L:
                ref = (''.join(l)).splitlines()[0]
                hyp = (''.join(l)).splitlines()[1]
                # Satzlaenge muss nur einmal ausgerechnet werden
                if n==1:
                    refCorpLen += len(ref.split())
                    hypCorpLen += len(hyp.split())

                # modified n-gram precision over corpus
                sum1 += BLEUcalc.matches(n, hyp, ref, True)
                sum2 += BLEUcalc.matches(n, hyp, hyp, False)

            Pn[n - 1] = sum1/sum2

        BP = BLEUcalc.brevity_penalty(hypCorpLen, refCorpLen)

        # rechne die Summe in der BLEU Gleichung aus
        tempSum = 0
        for p in Pn:
            tempSum += (1 / N) * math.log(p)

        res = BP * math.exp(tempSum)
        return res
