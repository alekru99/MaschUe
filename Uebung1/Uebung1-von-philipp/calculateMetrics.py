import argparse
import pickle
import BLEUcalc
import WERPERcalc

# Aufgabe 2 wurde erweitert

# Kommandozeilen Parameter deligieren
parser = argparse.ArgumentParser()
parser.add_argument("-ref", "--reference", required=True, help="Reference translation")
parser.add_argument("-hyp", "--hypothesis", required= True, help="Hypothesis translation")
parser.add_argument("-N", "--Number", type=int, nargs="?", help="Constant for n-gram")
args = parser.parse_args()

if args.Number is not None:
    N = args.Number
    if N < 1 :
        print("You have to use a positive Integer for N. For wrong input N will have standard value 4.")
        N = 4
else:
    N = 4

# Uebersetzungen oeffnen und einlesen
ref = open(args.reference, 'r', encoding="utf-8")
hyp = open(args.hypothesis, 'r', encoding="utf-8")

# Referenz und Hypothese zeilenweise in einer Liste speichern
l = list(zip(ref, hyp))

# Liste in save datei speichern
pickle.dump(l, open("save.pkl", "wb"))

# geoeffnete Uebersetzungen schliessen
ref.close()
hyp.close()

wer = WERPERcalc.WERcalc.wer()
print("WER:", wer)

per = WERPERcalc.PERcalc.per()
print("PER:", per)

bleu = BLEUcalc.BLEUcalc.bleu(N)
print("BLEU:", bleu)

