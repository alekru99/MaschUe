#Eingabe der Form python3 N3.py Refkorpus Hypothesenkorpus

import argparse
import Levenshteinclass


#Programmbeginn
refer = []
hypot = []
Numberofsentence = 0
second=False
lessthanthree=True

parser = argparse.ArgumentParser()
parser.add_argument('-ref',"--reference", help="Hier soll ein Refernzkorpus wie newtest.en angegeben werden", type=argparse.FileType('r'), required=True, nargs=1)
parser.add_argument('-hyp',"--hypothesis", help="Hier soll EIN Hypothesenkorpus wie newtest.hyp1 angegeben werden", type=argparse.FileType('r'), required=True, nargs=1)
args = parser.parse_args()
for line in args.reference[0]:
    refer.append(line)
for line in args.hypothesis[0]:
    hypot.append(line)
Numberofsentence = int(input("Welcher Satz soll betrachtet werden? Wähle eine Zahl zwischen 0 und " +str(len(refer)-1)+ ".\n")) #Bis Hier wie Nr2
Levenshteinclass.Levenshtein(refer[Numberofsentence].split(),hypot[Numberofsentence].split(),True)



