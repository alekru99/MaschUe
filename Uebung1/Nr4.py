#Eingabe der Form python3 N4.py Refkorpus Hypothesenkorpus
import argparse
import Bleuclass

#Anfang Programm (wie Nr2)

refer = []
hypot = []
Numberofsentence = 0
second=False
lessthanthree=True

parser = argparse.ArgumentParser()
parser.add_argument('-ref',"--reference", help="Hier soll ein Refernzkorpus wie newtest.en angegeben werden", type=argparse.FileType('r'), required=True, nargs=1)
parser.add_argument('-hyp',"--hypothesis", help="Hier soll EIN Hypothesenkorpus wie newtest.hyp1 angegeben werden", type=argparse.FileType('r'), required=True, nargs=1)
parser.add_argument('-N', "--N", help="Hier kann ein N ausgewählt werden", type=int, required=False, nargs=1)
N=4
args = parser.parse_args()
for line in args.reference[0]:
    refer.append(line)
for line in args.hypothesis[0]:
    hypot.append(line)
if(args.N is not None):
    N=args.N[0]
if(N<1):
    N=4
    print("N zu klein default 4 verwendet")
#Wie Nr2 nur keine Satzabfrage
print("Der BLEU-Wert ist:",Bleuclass.BLEUcalc.BLEU(refer,hypot, N))
