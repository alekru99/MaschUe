def Levenshtein(refsatz,hypsatz,b):
    j = len(refsatz)
    k = len(hypsatz)
    a = [0] *(j+1)
    for i in range(j+1):
        a[i] = [0] * (k+1) #Erstellt Matrix a der Größe j+1 k+1 voller 0en
    for i2 in range(0,j+1): #i2 iteriert über Zeilen
        for i3 in range(0,k+1): #i3 iteriert über Spalten
            if i2==0: #Sonderfall 0te Zeile (0,0 ignorieren da schon mit 0 initialisiert) immer gleich linkem Wert +1
                if i3>0: 
                    a[i2][i3]=a[i2][i3-1]+1
            elif i3==0: #Sonderfall 0te Spalte immer gleich oberem Wert +1
                a[i2][i3]=a[i2-1][i3]+1
            else: #Fall: nicht am Rand
                if refsatz[i2-1]==hypsatz[i3-1]: #Überprüfen auf Match
                    a[i2][i3]=a[i2-1][i3-1]
                else: #sonst Substitution
                    a[i2][i3]=a[i2-1][i3-1]+1
                a[i2][i3]=min((a[i2][i3]),(a[i2][i3-1]+1),(a[i2-1][i3]+1)) #Minimum von aktueller zelle (enthällt m bzw. s Wert schon) deletion und insertion
    if b:        
        print ("Die Levenshteindistanz ist:",a[j][k]) #Levenshteindistanz in unterer rechter Ecke
    if b:
        i2=j
        i3=k
        veraend=""
        while i2>0 or i3>0: #Rückwärtsiteration von unten rechts bis 0,0
            if(i2==0): #Sonderfall oberer Rand
                veraend="d"*i3 + veraend
                i3=0
            elif(i3==0): #Sonderfall linker Rand
                veraend="i"*i2 + veraend
                i2=0
            else: #Sonst suche nach minimalem Wert links, links-oben, oben
                minimum=min(a[i2-1][i3],a[i2-1][i3-1],a[i2][i3-1])
                if minimum==a[i2-1][i3-1]: #überprüfe position dieses Minimums und einfügen an Anfang von String (m und s bevorzugen)
                    if minimum==a[i2][i3]: #überprüfe ob m oder s
                        veraend="m"+veraend
                    else:
                        veraend="s"+veraend
                    i2-=1
                    i3-=1
                elif minimum==a[i2-1][i3]:
                    veraend="i"+veraend
                    i2-=1
                else:
                    veraend="d"+veraend
                    i3-=1
        print("\n Anpassungen: \n")
        indexref=0
        indexhyp=0        
        for changment in veraend:
            if changment=="m":
                print("Match of",refsatz[indexref],"and",hypsatz[indexhyp])
                indexref+=1
                indexhyp+=1
            elif changment=="s":
                print("Substitution of",hypsatz[indexhyp],"with",refsatz[indexref])
                indexref+=1
                indexhyp+=1
            elif changment=="d":
                print("Deletion of",hypsatz[indexhyp])
                indexhyp+=1
            elif changment=="i":
                print("Insertion of",refsatz[indexref])
                indexref+=1
            else: 
                print("something went wrong")
    if not b:
        return a[j][k]
