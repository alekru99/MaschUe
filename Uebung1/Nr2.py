#Eingabe der Form python3 N2.py Refkorpus Hypothesenkorpus

import argparse
def Einlesen(r,h):
    refsatz = r.split() #Splitet den Satz an den Leerzeichen auf und erstellt eine Liste
    hypsatz = h.split() #Analog
    print ("Referenzsatz:\n",refsatz,"\nHypothese:\n",hypsatz,"\n")

    
#Hier startet das Programm. Es liest Zwei Korpusse ein und fragt welcher Satz betrachtet werden soll. Dieser wird eingelesen und als Liste ausgegeben.
refer = [] #Enthält den Refernzkorpus als Liste über die einzelnen Zeilen
hypot = [] #Analog zu ref
Numberofsentence = 0
second=False #Wahr, nach einlesen der Referenz
lessthanthree=True #In diesem Programm soll nur eine Hypothese eingelesen werden! Falsch, nach einlesen der ersten Hypothese

parser = argparse.ArgumentParser()
parser.add_argument('-ref',"--reference", help="Hier soll ein Refernzkorpus wie newtest.en angegeben werden", type=argparse.FileType('r'), nargs=1)
parser.add_argument('-hyp',"--hypothesis", help="Hier soll EIN Hypothesenkorpus wie newtest.hyp1 angegeben werden", type=argparse.FileType('r'), nargs=1)
args = parser.parse_args()
for line in args.reference[0]:
    refer.append(line)
for line in args.hypothesis[0]:
    hypot.append(line)
Numberofsentence = int(input("Welcher Satz soll betrachtet werden? Wähle eine Zahl zwischen 0 und " +str(len(refer)-1)+ ".\n")) #Fragt nach gewünschter Satznummer
Einlesen(refer[Numberofsentence],hypot[Numberofsentence])



